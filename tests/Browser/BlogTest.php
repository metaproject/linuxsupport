<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BlogTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
            $this->browse(function (Browser $browser) {
                $browser->visit('/blog');
                $browser->type('#search input', 'Quisque')
                    ->click('#search a')
                    ->assertSee('Quisque')->assertDontSee('Ut tristique tellus');
            });
    }
}
