<?php

namespace Tests\Unit\Blog;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Dusk\DuskServiceProvider;

class BlogSearchTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->json('get', '/blog/', ['sections' => ['blog'],'string'=>'lorem']);

        $response->assertStatus(200);
    }
}
