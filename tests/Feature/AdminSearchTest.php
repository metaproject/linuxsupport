<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App;

class AdminSearchTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = factory(App\User::class, 'admin')->create();
        $response = $this->actingAs($user)->json('POST', '/admin/search',['order'=>1,'sections'=>['orders']]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'orders' => ['current_page'=>1],
            ]);
    }
}
