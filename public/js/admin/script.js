var managerName = document.querySelectorAll('.order-manager');

var managerOption = document.querySelector('#managerOption');
managerName.forEach(function(e){
    var selectedManager = e.nextSibling.nextSibling;
    e.innerText = selectedManager.value;
    selectedManager.addEventListener('click', function(){
        e.innerText = selectedManager.value;
    });
});

var statusName = document.querySelectorAll('.order-status');
var statusOption = document.querySelector('#statusOption');
statusName.forEach(function(e){
    var selectedStatus = e.nextSibling.nextSibling;
    e.innerText = selectedStatus.value;
    selectedStatus.addEventListener('click', function(){
        e.innerText = selectedStatus.value;
    });
});

var addForm1 = document.querySelector('#addform1');
var form1 = document.querySelector('#form1');
form1.classList.add('none');
addForm1.addEventListener('click', function(){
    form1.classList.toggle('none');
});
var addForm1Button = document.querySelector('#addform1button');
addForm1Button.addEventListener('click', function(){
    form1.classList.toggle('none');
});

var addForm2 = document.querySelector('#addform2');
var form2 = document.querySelector('#form2');
form2.classList.add('none');
addForm2.addEventListener('click', function(){
    form2.classList.toggle('none');
});
var addForm2Button = document.querySelector('#addform2button');
addForm2Button.addEventListener('click', function(){
    form2.classList.toggle('none');
});