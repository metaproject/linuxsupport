<?php

namespace App\Http\Controllers\Payment;

use Illuminate\Http\Request;
use App\PaymentMethodsFactory;
use App;
use App\Http\Controllers\Controller;
use Validator;

class PaymentController extends Controller
{
    protected $payment;
    public function pay(Request $request, $method)
    {
        $order_ses=session('order')['order'];
        $client_ses=session('order')['client'];
        $this->PaymentValidator(array_merge($order_ses,$client_ses))->validate();
        $amount=(isset($order_ses['cost'])?$order_ses['cost']:App\Order_type::where('id',$order_ses['type'])->first()->cost);
        $director=new PaymentMethodsFactory\PaymentDirector($method);
        $this->payment=$director->getPayment();
        $pay_success=$this->payment->payment($request,$order_ses,$amount);
        if($pay_success){
            $userId = Auth::id();
            $client=App\Client::firstorNew(['user_id'=>$userId]);
            $client->communication_channel_id=$client_ses['prefer'];
            $client->save();
            $communication=App\Communication_coordinate::firstOrNew(['communication_chanel_id'=>$client_ses['prefer'],'user_id'=>$userId]);
            $communication->coordinate = ($client_ses['prefer']!=5)?$client_ses['phone']:Auth::user()->email;
            $communication->save();
            $order = App\Order::firstorNew(['client_id'=>$client->id]);
            $order->type_id=$order_ses['type'];
            $order->status_id = 1;
            $order->renewal = ($order_ses['type']==1)?1:0;
            $order->payment_date=Carbon::now();
            $order->save();
            $payment_list=new App\Payment;
            $payment_list->ammount=$amount;
            $payment_list->order_id=$order->id;
            $payment_list->client_id=$client->id;
            $payment_list->payment_date=Carbon::now();
            $payment_list->save();
            if ($order->created_at==Carbon::now()) {
                //new handling
                $newHandling=new App\Handling;
                $newHandling->problem = $request->problem;
                $newHandling->order_id = $order->id;
                $newHandling->handling = 1;
                $newHandling->save();
            }
            return response()->json(['success' => 'Order added successfully']);
        }else
        {
            return response()->json(['success' => 'Payment wrong']);
        }
    }
    protected function PaymentValidator(array $data){
        return Validator::make($data, [
            /*'card_number'=>'required',
            'cvv'=>'required',
            'exp_month'=>'required',
            'exp_year'=>'required',*/
            'phone' => 'required|int',
            'prefer'=>'required|int|exists:communications,id',
            'type'=>'required|int|exists:orders_types,id',
            'cost'=>'float'
        ]);
    }
}
