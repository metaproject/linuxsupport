<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'google' => [
        'client_id' => '891170871841-qmhhim272qu1vc10hspdkrgj179o5pac.apps.googleusercontent.com',
        'client_secret' => 'F_kWf8YNOZMXz0oXS3YkUjz1',
        'redirect' => 'http://linuxsupport.metaproject.pro/socialite/google/callback', // Ссылка на перенаправление при удачной авторизации (3)
    ],
    'facebook' => [
        'client_id' => '2200268406668032',
        'client_secret' => '56f87eb75b0923dacb2c51013bf68356',
        'redirect' => 'http://linuxsupport.metaproject.pro/socialite/facebook/callback',
    ],
    'stripe' => [
        'secret' => 'your-stripe-key-here',
    ],

];
