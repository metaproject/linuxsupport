let mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(['resources/assets/js/app.js','resources/assets/js/my.js'], 'public/js').sourceMaps()
    .js('resources/assets/js/script.js', 'public/js').sourceMaps()
    .js('resources/assets/js/admin.js', 'public/js').sourceMaps()
   .sass('resources/assets/sass/app.scss', 'public/css/styles')
    .sass('resources/assets/sass/my.scss', 'public/css/styles');
mix.styles(['public/css/styles/app.css','public/css/styles/my.css'],'public/css/app.css').sourceMaps();
mix.autoload({
    jQuery: 'jquery',
    $: 'jquery',
    jquery: 'jquery'
});
