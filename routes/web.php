<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'/', 'middleware' => ['locale']],   function() { #language group
    Route::get('/', 'HomeController@index')->name('home');

    Auth::routes();
    Route::post('/ulogin', 'UloginController@login');
//main page
    Route::post('/order/pay', 'OrderController@orderPay');
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/order/pay', 'OrderController@PayForOrder')->name('payForOrder');
    });
//blog
    Route::group(['prefix' => 'blog'], function () {
        Route::get('/', 'Blog\BlogController@index')->name('blogHome');
        Route::get('/{article}', 'Blog\BlogController@article')->name('blogArticle');
        Route::get('/search/{search?}', 'Blog\BlogController@index');
        Route::post('/search', 'Blog\BlogController@search');
    });
//personal
    Route::group(['prefix' => 'personal','middleware' => ['auth']], function () {
        Route::get('/', 'Personal\PersonalController@index')->name('personalHome');
        Route::get('/changePlan/{order_id}/{new_plan}', 'OrderController@changePlan')->name('changePlan');
        Route::post('/change', 'Personal\PersonalController@changePersonalData');
    });
    //activation
    Route::get('activate/newmessage', 'Auth\ActivateController@newMessage')->name('newActivateMess');
    Route::get('activate/{id}/{token}', 'Auth\ActivateController@activation')->name('activation');
    //information
    Route::group(['prefix' => 'inform'], function () {
        Route::get('/', 'Inform\InformController@index')->name('InformHome');
    });
    //authorization
    Route::post('/login', 'Auth\PopupAuthController@login');
        //social
    Route::get(
        '/socialite/{provider}',
        [
            'as' => 'socialite.auth',
            function ( $provider ) {
                return \Socialite::driver( $provider )->redirect();
            }
        ]
    );

    Route::get('/socialite/{provider}/callback', 'Auth\SocialAuthController@auth');
    //logout
    Route::post('/personal/logout', 'Auth\LogoutController@logout');
    //register
    Route::post('/register', 'Auth\PopupRegisterController@register');
//admin authorization
    Route::get('/admin/login', array(
        'middleware' => 'redirectIfAuthAdmin:manager,admin',
        'uses' => 'Admin\AdminAuthController@getLogin'
    ));
    Route::post('/admin/login', 'Admin\AdminAuthController@postLogin')->name('adminAuth');
//payment
    Route::post('/payment/{method}', 'Payment\PaymentController@pay');
//admin panel
    Route::group(['prefix' => 'admin', 'middleware' => ['inGroup:manager,admin', 'adminRoles']], function () {
        //get
        /*Route::get('/', 'Admin\PanelController@index')->name('adminHome');*/
        Route::get('/', 'Admin\PanelController@orders')->name('adminOrders');
        Route::get('/archive', 'Admin\PanelController@archive')->name('adminArchive');
        Route::get('/clients', 'Admin\PanelController@clients')->name('adminClients');
        Route::get('/managers', 'Admin\PanelController@managers')->name('adminManagers');
        Route::get('/blog', 'Admin\PanelController@blog')->name('adminBlog');
        Route::get('/notes', 'Admin\PanelController@notes')->name('adminNotes');
        Route::get('/fields', 'Admin\PanelController@fields')->name('adminFields');
        //requests
        //get
        Route::get('/orders/search/{search?}', 'Admin\PanelController@orders');
        //post
        Route::post('/search', ['middleware'=>'search','uses' =>'Admin\PanelController@search']);
        Route::post('/managers/add', 'Admin\PanelController@addManager');
        Route::post('/managers/delete', 'Admin\PanelController@deleteManager');
        Route::post('/blog/add', 'Admin\PanelController@addArticle');
        Route::post('/blog/update', 'Admin\PanelController@updateArticle');
        Route::post('/blog/delete', 'Admin\PanelController@deleteArticle');
        Route::post('/notes/add', 'Admin\PanelController@addNote');
        Route::post('/notes/update', 'Admin\PanelController@updateNote');
        Route::post('/notes/delete', 'Admin\PanelController@deleteNote');
        Route::post('/notes/addType', 'Admin\PanelController@addNoteType');
        Route::post('/notes/deleteType', 'Admin\PanelController@deleteNoteType');
        Route::post('/fields/add', 'Admin\PanelController@addField');
        Route::post('/fields/update', 'Admin\PanelController@updateField');
        Route::post('/fields/delete', 'Admin\PanelController@deleteField');
    });
//set locale
    Route::get('/setlocale/{locale}', function ($locale) {

        if (in_array($locale, \Config::get('app.locales'))) {   # check that chosen lang available
            Session::put('locale', $locale);                    # set language in session
        }

        return redirect()->back();                              # go back

    })->name('changeLanguage');
});
