<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTagsArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tags_articles', function(Blueprint $table)
		{
			$table->foreign('tag_id', 'tags_articles_ibfk_1')->references('id')->on('tags')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('article_id', 'tags_articles_ibfk_2')->references('id')->on('blog')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tags_articles', function(Blueprint $table)
		{
			$table->dropForeign('tags_articles_ibfk_1');
			$table->dropForeign('tags_articles_ibfk_2');
		});
	}

}
