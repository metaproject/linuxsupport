<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHandlingsManagersCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('handlings_managers_comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('handling_id')->unsigned()->index('order_id');
			$table->integer('manager_id')->unsigned()->index('manager_id');
			$table->text('comment', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('handlings_managers_comments');
	}

}
