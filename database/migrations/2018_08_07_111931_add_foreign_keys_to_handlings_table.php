<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHandlingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('handlings', function(Blueprint $table)
		{
			$table->foreign('order_id', 'handlings_ibfk_1')->references('id')->on('orders')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('handlings', function(Blueprint $table)
		{
			$table->dropForeign('handlings_ibfk_1');
		});
	}

}
