<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHandlingsManagersCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('handlings_managers_comments', function(Blueprint $table)
		{
			$table->foreign('handling_id', 'handlings_managers_comments_ibfk_1')->references('id')->on('handlings')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('manager_id', 'handlings_managers_comments_ibfk_2')->references('id')->on('managers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('handlings_managers_comments', function(Blueprint $table)
		{
			$table->dropForeign('handlings_managers_comments_ibfk_1');
			$table->dropForeign('handlings_managers_comments_ibfk_2');
		});
	}

}
