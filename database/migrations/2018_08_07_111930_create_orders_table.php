<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_id')->unsigned()->index('client_id');
			$table->boolean('type_id')->index('order_type_id');
			$table->boolean('status_id')->index('order_status_id');
			$table->integer('rate_id')->unsigned()->nullable()->index('order_rate_id');
			$table->boolean('renewal')->default(0);
			$table->dateTime('payment_date')->nullable();
			$table->integer('manager_id')->unsigned()->nullable()->index('manager_id');
			$table->timestamps();
			$table->dateTime('done_at')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
