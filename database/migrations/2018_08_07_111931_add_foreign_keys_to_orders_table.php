<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->foreign('client_id', 'orders_ibfk_1')->references('id')->on('clients')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('rate_id', 'orders_ibfk_2')->references('id')->on('orders_rates')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('status_id', 'orders_ibfk_3')->references('id')->on('orders_statuses')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('rate_id', 'orders_ibfk_4')->references('id')->on('orders_rates')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('type_id', 'orders_ibfk_5')->references('id')->on('orders_types')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('manager_id', 'orders_ibfk_6')->references('id')->on('managers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->dropForeign('orders_ibfk_1');
			$table->dropForeign('orders_ibfk_2');
			$table->dropForeign('orders_ibfk_3');
			$table->dropForeign('orders_ibfk_4');
			$table->dropForeign('orders_ibfk_5');
			$table->dropForeign('orders_ibfk_6');
		});
	}

}
