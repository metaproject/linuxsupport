<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommunicationsCoordinatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('communications_coordinates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('communication_channel_id')->index('communication_channel_id');
			$table->integer('user_id')->unsigned()->index('client_id');
			$table->string('coordinate');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('communications_coordinates');
	}

}
