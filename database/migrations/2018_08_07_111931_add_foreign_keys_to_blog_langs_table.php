<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBlogLangsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('blog_langs', function(Blueprint $table)
		{
			$table->foreign('language_id', 'blog_langs_ibfk_1')->references('id')->on('languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('blog_id', 'blog_langs_ibfk_2')->references('id')->on('blog')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('blog_langs', function(Blueprint $table)
		{
			$table->dropForeign('blog_langs_ibfk_1');
			$table->dropForeign('blog_langs_ibfk_2');
		});
	}

}
