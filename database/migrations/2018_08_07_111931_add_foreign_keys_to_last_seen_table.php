<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLastSeenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('last_seen', function(Blueprint $table)
		{
			$table->foreign('subject_type_id', 'last_seen_ibfk_1')->references('id')->on('last_seen_types')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id', 'last_seen_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('last_seen', function(Blueprint $table)
		{
			$table->dropForeign('last_seen_ibfk_1');
			$table->dropForeign('last_seen_ibfk_2');
		});
	}

}
