<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrdersTypeLangTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders_type_lang', function(Blueprint $table)
		{
			$table->foreign('language_id', 'orders_type_lang_ibfk_1')->references('id')->on('languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('order_type_id', 'orders_type_lang_ibfk_2')->references('id')->on('orders_types')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders_type_lang', function(Blueprint $table)
		{
			$table->dropForeign('orders_type_lang_ibfk_1');
			$table->dropForeign('orders_type_lang_ibfk_2');
		});
	}

}
