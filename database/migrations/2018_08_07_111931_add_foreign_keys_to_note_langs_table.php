<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNoteLangsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('note_langs', function(Blueprint $table)
		{
			$table->foreign('language_id', 'note_langs_ibfk_1')->references('id')->on('languages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('note_id', 'note_langs_ibfk_2')->references('id')->on('notes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('note_langs', function(Blueprint $table)
		{
			$table->dropForeign('note_langs_ibfk_1');
			$table->dropForeign('note_langs_ibfk_2');
		});
	}

}
