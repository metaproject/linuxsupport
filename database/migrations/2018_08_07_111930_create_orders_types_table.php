<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders_types', function(Blueprint $table)
		{
			$table->boolean('id')->primary();
			$table->integer('computers')->unsigned();
			$table->float('cost', 10, 0)->unsigned();
			$table->string('period', 50);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders_types');
	}

}
