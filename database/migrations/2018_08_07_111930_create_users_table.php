<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->nullable()->unique();
			$table->string('password');
			$table->boolean('role_id')->default(3);
			$table->string('photo')->nullable();
			$table->boolean('active')->default(0);
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->string('stripe_id', 50)->nullable();
			$table->string('card_brand', 50)->nullable();
			$table->string('card_last_four', 4)->nullable();
			$table->dateTime('trial_ends_at')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
