<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCommunicationsCoordinatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('communications_coordinates', function(Blueprint $table)
		{
			$table->foreign('communication_channel_id', 'communications_coordinates_ibfk_1')->references('id')->on('communications')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id', 'communications_coordinates_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('communications_coordinates', function(Blueprint $table)
		{
			$table->dropForeign('communications_coordinates_ibfk_1');
			$table->dropForeign('communications_coordinates_ibfk_2');
		});
	}

}
