<?php

use Illuminate\Database\Seeder;

class CommunicationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('communications')->delete();
        
        \DB::table('communications')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Telegram',
                'image' => 'images/icon-telegram.png',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'WhatsApp',
                'image' => 'images/icon-whatsapp.png',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Viber',
                'image' => 'images/icon-viber.png',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Skype',
                'image' => 'images/icon-skype.png',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'E-mail',
                'image' => 'images/icon-email.png',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Phone',
                'image' => 'images/icon-call-BW.svg',
            ),
        ));
        
        
    }
}