<?php

use Illuminate\Database\Seeder;

class BlogLangsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('blog_langs')->delete();
        
        \DB::table('blog_langs')->insert(array (
            0 => 
            array (
                'id' => 3,
                'language_id' => 1,
                'blog_id' => 1,
                'title' => 'Lorem ipsum dolor sit ame',
                'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim nibh in neque blandit sollicitudin. Vivamus sed odio eget risus mollis placerat et quis felis. Integer convallis neque sit amet augue bibendum, a blandit purus interdum. Quisque eleifend, massa pellentesque posuere ultricies, felis ipsum porttitor felis, posuere rhoncus ligula ligula lacinia mi. Nulla tristique erat in mauris tristique aliquet. Suspendisse aliquet facilisis leo ac molestie. Vestibulum sodales nibh quam, ut faucibus nisl rhoncus id. Vestibulum iaculis, nisi nec vestibulum varius, turpis libero scelerisque purus, et ultricies dolor metus nec erat. Mauris ullamcorper augue et condimentum auctor. Cras sapien felis, rutrum at risus sit amet, condimentum vehicula ex. Maecenas pharetra iaculis justo, lobortis ornare ligula bibendum cursus.',
            ),
            1 => 
            array (
                'id' => 4,
                'language_id' => 1,
                'blog_id' => 2,
                'title' => 'Ut tristique tellus',
                'text' => 'Ut tristique tellus non feugiat sollicitudin. Aliquam efficitur neque quam, nec mollis massa rutrum a. Aenean varius vulputate nunc, ac dignissim dui. Integer fringilla posuere felis vitae gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec sit amet est orci. Aliquam interdum felis lacus, et porttitor turpis ultricies et. Proin dapibus et ipsum sit amet maximus. Cras scelerisque elit vel venenatis gravida. Cras sed est diam. Aenean vulputate malesuada dolor, nec tempor felis.',
            ),
        ));
        
        
    }
}