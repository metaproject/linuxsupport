<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('clients')->delete();
        
        \DB::table('clients')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'communication_channel_id' => 5,
                'created_at' => '2018-07-03 00:00:00',
                'updated_at' => '2018-07-03 00:00:00',
            ),
        ));
        
        
    }
}