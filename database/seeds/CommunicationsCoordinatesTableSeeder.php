<?php

use Illuminate\Database\Seeder;

class CommunicationsCoordinatesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('communications_coordinates')->delete();
        
        \DB::table('communications_coordinates')->insert(array (
            0 => 
            array (
                'id' => 1,
                'communication_channel_id' => 5,
                'user_id' => 1,
                'coordinate' => 'sozin1991@gmail.com',
                'created_at' => '2018-07-03 00:00:00',
                'updated_at' => '2018-07-03 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'communication_channel_id' => 6,
                'user_id' => 1,
                'coordinate' => '11111',
                'created_at' => '2018-07-10 00:00:00',
                'updated_at' => '2018-07-10 00:00:00',
            ),
        ));
        
        
    }
}