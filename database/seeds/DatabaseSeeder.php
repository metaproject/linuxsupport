<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BlogTableSeeder::class);
        $this->call(BlogLangsTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
        $this->call(CommunicationsTableSeeder::class);
        $this->call(CommunicationsCoordinatesTableSeeder::class);
        $this->call(ComputersTableSeeder::class);
        $this->call(CustomFieldsTableSeeder::class);
        $this->call(HandlingsTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(ManagersTableSeeder::class);
        $this->call(MenuTypesTableSeeder::class);
        $this->call(MigrationsTableSeeder::class);
        $this->call(NavigationTableSeeder::class);
        $this->call(NotesTableSeeder::class);
        $this->call(NoteLangsTableSeeder::class);
        $this->call(NoteTypesTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(OrdersManagersCommentsTableSeeder::class);
        $this->call(OrdersRatesTableSeeder::class);
        $this->call(OrdersStatusesTableSeeder::class);
        $this->call(OrdersTypesTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(SubscriptionsTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(TagsArticlesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}
