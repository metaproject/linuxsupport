<?php

use Illuminate\Database\Seeder;

class BlogTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('blog')->delete();
        
        \DB::table('blog')->insert(array (
            0 => 
            array (
                'id' => 1,
                'author_id' => 1,
                'image' => 'images/img-blog1.png',
                'recomended' => 1,
                'created_at' => '2018-07-05 00:00:00',
                'updated_at' => '2018-07-05 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'author_id' => 1,
                'image' => 'images/img-blog2.png',
                'recomended' => 0,
                'created_at' => '2018-07-05 00:00:00',
                'updated_at' => '2018-07-05 00:00:00',
            ),
        ));
        
        
    }
}