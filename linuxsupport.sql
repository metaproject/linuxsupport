-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 10, 2018 at 01:45 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.2.7-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `linuxsupport`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recomended` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `author_id`, `image`, `recomended`, `created_at`, `updated_at`) VALUES
(1, 1, 'images/img-blog1.png', 1, '2018-07-04 21:00:00', '2018-07-04 21:00:00'),
(2, 1, 'images/img-blog2.png', 1, '2018-07-04 21:00:00', '2018-07-04 21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `blog_langs`
--

CREATE TABLE `blog_langs` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_langs`
--

INSERT INTO `blog_langs` (`id`, `language_id`, `blog_id`, `title`, `text`) VALUES
(3, 1, 1, 'Lorem ipsum dolor sit ame', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim nibh in neque blandit sollicitudin. Vivamus sed odio eget risus mollis placerat et quis felis. Integer convallis neque sit amet augue bibendum, a blandit purus interdum. Quisque eleifend, massa pellentesque posuere ultricies, felis ipsum porttitor felis, posuere rhoncus ligula ligula lacinia mi. Nulla tristique erat in mauris tristique aliquet. Suspendisse aliquet facilisis leo ac molestie. Vestibulum sodales nibh quam, ut faucibus nisl rhoncus id. Vestibulum iaculis, nisi nec vestibulum varius, turpis libero scelerisque purus, et ultricies dolor metus nec erat. Mauris ullamcorper augue et condimentum auctor. Cras sapien felis, rutrum at risus sit amet, condimentum vehicula ex. Maecenas pharetra iaculis justo, lobortis ornare ligula bibendum cursus.'),
(4, 1, 2, 'Ut tristique tellus', 'Ut tristique tellus non feugiat sollicitudin. Aliquam efficitur neque quam, nec mollis massa rutrum a. Aenean varius vulputate nunc, ac dignissim dui. Integer fringilla posuere felis vitae gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec sit amet est orci. Aliquam interdum felis lacus, et porttitor turpis ultricies et. Proin dapibus et ipsum sit amet maximus. Cras scelerisque elit vel venenatis gravida. Cras sed est diam. Aenean vulputate malesuada dolor, nec tempor felis.');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `communication_channel_id` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `user_id`, `communication_channel_id`, `created_at`, `updated_at`) VALUES
(1, 1, 5, '2018-07-02 21:00:00', '2018-07-02 21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `communications`
--

CREATE TABLE `communications` (
  `id` tinyint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `communications`
--

INSERT INTO `communications` (`id`, `name`, `image`) VALUES
(1, 'Telegram', 'images/icon-telegram.png'),
(2, 'WhatsApp', 'images/icon-whatsapp.png'),
(3, 'Viber', 'images/icon-viber.png'),
(4, 'Skype', 'images/icon-skype.png'),
(5, 'E-mail', 'images/icon-email.png'),
(6, 'Phone', 'images/icon-call-BW.svg');

-- --------------------------------------------------------

--
-- Table structure for table `communications_coordinates`
--

CREATE TABLE `communications_coordinates` (
  `id` int(10) UNSIGNED NOT NULL,
  `communication_channel_id` tinyint(1) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `coordinate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `communications_coordinates`
--

INSERT INTO `communications_coordinates` (`id`, `communication_channel_id`, `user_id`, `coordinate`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 'sozin1991@gmail.com', '2018-07-02 21:00:00', '2018-08-09 08:02:10'),
(3, 6, 1, '1111111111111', '2018-08-09 08:23:06', '2018-08-09 08:49:12'),
(4, 6, 30, '+380937531474', '2018-08-10 04:15:17', '2018-08-10 04:15:17');

-- --------------------------------------------------------

--
-- Table structure for table `computers`
--

CREATE TABLE `computers` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `computers`
--

INSERT INTO `computers` (`id`, `order_id`, `code`) VALUES
(1, 1, '34pe');

-- --------------------------------------------------------

--
-- Table structure for table `custom_fields`
--

CREATE TABLE `custom_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `value` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `custom_fields`
--

INSERT INTO `custom_fields` (`id`, `value`, `alias`) VALUES
(1, '380964556789', 'our_phone');

-- --------------------------------------------------------

--
-- Table structure for table `handlings`
--

CREATE TABLE `handlings` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `handling` int(11) NOT NULL,
  `problem` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `done` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `handlings`
--

INSERT INTO `handlings` (`id`, `order_id`, `handling`, `problem`, `done`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Lorem Ipsum', 0, '2018-07-17 21:00:00', '2018-07-17 21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `handlings_managers_comments`
--

CREATE TABLE `handlings_managers_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `handling_id` int(10) UNSIGNED NOT NULL,
  `manager_id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `handlings_managers_comments`
--

INSERT INTO `handlings_managers_comments` (`id`, `handling_id`, `manager_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Lorem', '2018-07-18 21:00:00', '2018-07-18 21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`) VALUES
(1, 'en'),
(2, 'ru');

-- --------------------------------------------------------

--
-- Table structure for table `last_seen`
--

CREATE TABLE `last_seen` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `subject_type_id` int(10) UNSIGNED NOT NULL,
  `time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `last_seen`
--

INSERT INTO `last_seen` (`id`, `user_id`, `subject_type_id`, `time`) VALUES
(1, 1, 1, '2018-08-10 06:41:18');

-- --------------------------------------------------------

--
-- Table structure for table `last_seen_types`
--

CREATE TABLE `last_seen_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `last_seen_types`
--

INSERT INTO `last_seen_types` (`id`, `name`) VALUES
(1, 'orders');

-- --------------------------------------------------------

--
-- Table structure for table `managers`
--

CREATE TABLE `managers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `managers`
--

INSERT INTO `managers` (`id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 2, '2018-07-16 21:00:00', '2018-07-16 21:00:00'),
(2, 1, '2018-08-05 21:00:00', '2018-08-05 21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `menu_types`
--

CREATE TABLE `menu_types` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_types`
--

INSERT INTO `menu_types` (`id`, `name`) VALUES
(1, 'Admin Panel');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_07_19_145218_create_blog_langs_table', 1),
(2, '2018_07_19_145218_create_blog_table', 1),
(3, '2018_07_19_145218_create_clients_table', 1),
(4, '2018_07_19_145218_create_communications_coordinates_table', 1),
(5, '2018_07_19_145218_create_communications_table', 1),
(6, '2018_07_19_145218_create_custom_fields_table', 1),
(7, '2018_07_19_145218_create_handlings_table', 1),
(8, '2018_07_19_145218_create_languages_table', 1),
(9, '2018_07_19_145218_create_managers_table', 1),
(10, '2018_07_19_145218_create_menu_types_table', 1),
(11, '2018_07_19_145218_create_navigation_table', 1),
(12, '2018_07_19_145218_create_note_langs_table', 1),
(13, '2018_07_19_145218_create_note_types_table', 1),
(14, '2018_07_19_145218_create_notes_table', 1),
(15, '2018_07_19_145218_create_orders_managers_comments_table', 1),
(16, '2018_07_19_145218_create_orders_rates_table', 1),
(17, '2018_07_19_145218_create_orders_statuses_table', 1),
(18, '2018_07_19_145218_create_orders_table', 1),
(19, '2018_07_19_145218_create_orders_types_table', 1),
(20, '2018_07_19_145218_create_password_resets_table', 1),
(21, '2018_07_19_145218_create_roles_table', 1),
(22, '2018_07_19_145218_create_subscriptions_table', 1),
(23, '2018_07_19_145218_create_tags_articles_table', 1),
(24, '2018_07_19_145218_create_tags_table', 1),
(25, '2018_07_19_145218_create_users_table', 1),
(26, '2018_07_19_145219_add_foreign_keys_to_blog_langs_table', 1),
(27, '2018_07_19_145219_add_foreign_keys_to_blog_table', 1),
(28, '2018_07_19_145219_add_foreign_keys_to_clients_table', 1),
(29, '2018_07_19_145219_add_foreign_keys_to_communications_coordinates_table', 1),
(30, '2018_07_19_145219_add_foreign_keys_to_handlings_table', 1),
(31, '2018_07_19_145219_add_foreign_keys_to_managers_table', 1),
(32, '2018_07_19_145219_add_foreign_keys_to_navigation_table', 1),
(33, '2018_07_19_145219_add_foreign_keys_to_note_langs_table', 1),
(34, '2018_07_19_145219_add_foreign_keys_to_notes_table', 1),
(35, '2018_07_19_145219_add_foreign_keys_to_orders_managers_comments_table', 1),
(36, '2018_07_19_145219_add_foreign_keys_to_orders_rates_table', 1),
(37, '2018_07_19_145219_add_foreign_keys_to_orders_table', 1),
(38, '2018_07_19_145219_add_foreign_keys_to_tags_articles_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `navigation`
--

CREATE TABLE `navigation` (
  `id` int(11) NOT NULL,
  `menu_type_id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `navigation`
--

INSERT INTO `navigation` (`id`, `menu_type_id`, `name`, `url`, `parent_id`) VALUES
(2, 1, 'Orders', '/admin', NULL),
(3, 1, 'Archive', '/admin/archive', NULL),
(4, 1, 'Clients', '/admin/clients', NULL),
(5, 1, 'Managers', '/admin/managers', NULL),
(6, 1, 'Blog', '/admin/blog', NULL),
(7, 1, 'Notes', '/admin/notes', NULL),
(8, 1, 'Custom Fields', '/admin/fields', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `type_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 5, NULL, '2018-07-11 21:00:00', '2018-07-11 21:00:00'),
(2, 5, NULL, '2018-07-11 21:00:00', '2018-07-11 21:00:00'),
(3, 1, NULL, '2018-07-12 21:00:00', '2018-07-12 21:00:00'),
(4, 1, NULL, '2018-07-12 21:00:00', '2018-07-12 21:00:00'),
(5, 1, NULL, '2018-07-12 21:00:00', '2018-07-12 21:00:00'),
(6, 1, NULL, '2018-07-12 21:00:00', '2018-07-12 21:00:00'),
(7, 1, NULL, '2018-07-12 21:00:00', '2018-07-12 21:00:00'),
(8, 2, 'images/icon-workteam.svg', '2018-07-12 21:00:00', '2018-07-12 21:00:00'),
(9, 2, 'images/icon-24-hours.svg', '2018-07-12 21:00:00', '2018-07-12 21:00:00'),
(10, 2, 'images/icon-team.svg', '2018-07-12 21:00:00', '2018-07-12 21:00:00'),
(11, 3, 'images/photo-1.png', '2018-07-12 21:00:00', '2018-07-12 21:00:00'),
(12, 3, 'images/photo-2.png', '2018-07-12 21:00:00', '2018-07-12 21:00:00'),
(13, 3, 'images/photo-3.png', '2018-07-12 21:00:00', '2018-07-12 21:00:00'),
(14, 3, 'images/photo-1.png', '2018-07-17 21:00:00', '2018-07-17 21:00:00'),
(15, 5, NULL, '2018-07-17 21:00:00', '2018-07-17 21:00:00'),
(16, 5, NULL, '2018-07-17 21:00:00', '2018-07-17 21:00:00'),
(17, 5, NULL, '2018-07-17 21:00:00', '2018-07-17 21:00:00'),
(18, 5, NULL, '2018-07-17 21:00:00', '2018-07-17 21:00:00'),
(19, 5, NULL, '2018-07-17 21:00:00', '2018-07-17 21:00:00'),
(20, 5, NULL, '2018-07-17 21:00:00', '2018-07-17 21:00:00'),
(21, 6, 'images/advantage-1.png', '2018-07-26 21:00:00', '2018-07-26 21:00:00'),
(22, 6, 'images/advantage-2.png', '2018-07-26 21:00:00', '2018-07-26 21:00:00'),
(23, 6, 'images/advantage-3.png', '2018-07-26 21:00:00', '2018-07-26 21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `note_langs`
--

CREATE TABLE `note_langs` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `note_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `note_langs`
--

INSERT INTO `note_langs` (`id`, `language_id`, `note_id`, `title`, `text`) VALUES
(1, 1, 1, 'Which Linux distributions do you support?', 'Our standard support contracts cover the following Linux distributions: Debian, Red Hat, CentOS, Ubuntu, Scientific Linux. Generally we only support the current release(s) of those distributions, however, if you have specific version requirements or you would like to discuss support of another distribution, please do contact us.'),
(2, 1, 2, 'How do I get into safe mode?', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam voluptatum nemo, similique, laboriosam earum minus excepturi aliquam placeat consectetur a sapiente culpa nihil quidem, nulla illo omnis ex architecto voluptatem.'),
(3, 1, 3, 'Application', 'Request help and we will contact you in the most comfortbale way'),
(4, 1, 4, 'Description of the problem', 'You will describe your problem to our assistant'),
(5, 1, 5, 'Get solutions', 'In less than 10 minutes we\'ll return back to you with ready solution'),
(6, 1, 6, 'Connect via TeamViewer', 'We connect to your computer and do all the fixes by ourserlves'),
(7, 1, 7, 'Step-to-Step Instruction', 'When it\'s not possible to connect directly, will guide you and explain every action you must do to fix the issue.'),
(8, 1, 8, '12 specialists', 'graduated specialist reday to make your life more comfortable'),
(9, 1, 9, '24 hours and 7 days', 'hours and 7 days a week we are ready to help'),
(10, 1, 10, '545 clients', 'happy client\'s by this far'),
(11, 1, 11, 'Lisa Strong', 'Thanks for the help! It really helped me get a grasp on the basics of linux in an easy to use tutorial. It was very interactive which made the experience easy to finish all in one sitting because it made it more enjoyable. I could go on writing a praise for you but I’ll sum it up in just one word “Exceptional”.'),
(12, 1, 12, 'Kevin Shelton', 'It really helped me get a grasp on the basics of linux in an easy to use tutorial. It was very interactive which made the experience easy to finish all in one sitting because it made it more enjoyable.'),
(13, 1, 13, 'Lisa Kwong', 'Thanks for the help! It really helped me get a grasp on the basics of linux in an easy to use tutorial. It was very interactive which made the experience easy to finish all in one sitting because it made it more enjoyable.'),
(14, 1, 14, 'Lisa Strong', 'Thanks for the help! It really helped me get a grasp on the basics of linux in an easy to use tutorial. It was very interactive which made the experience easy to finish all in one sitting because it made it more enjoyable. I could go on writing a praise for you but I’ll sum it up in just one word “Exceptional”.'),
(15, 1, 15, 'I can\'t delete a file because it is being used by Linux?', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam voluptatum nemo, similique, laboriosam earum minus excepturi aliquam placeat consectetur a sapiente culpa nihil quidem, nulla illo omnis ex architecto voluptatem.'),
(16, 1, 16, 'How can I recover a file from the recycle bin?', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam voluptatum nemo, similique, laboriosam earum minus excepturi aliquam placeat consectetur a sapiente culpa nihil quidem, nulla illo omnis ex architecto voluptatem.'),
(17, 1, 17, 'How can I update my Linux computer?', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam voluptatum nemo, similique, laboriosam earum minus excepturi aliquam placeat consectetur a sapiente culpa nihil quidem, nulla illo omnis ex architecto voluptatem.'),
(18, 1, 18, 'Is it safe to turn off a Linux computer without doing a shut down?', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam voluptatum nemo, similique, laboriosam earum minus excepturi aliquam placeat consectetur a sapiente culpa nihil quidem, nulla illo omnis ex architecto voluptatem.'),
(19, 1, 19, 'Can a virus damage computer hardware?', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam voluptatum nemo, similique, laboriosam earum minus excepturi aliquam placeat consectetur a sapiente culpa nihil quidem, nulla illo omnis ex architecto voluptatem.'),
(20, 1, 20, 'If I format or erase my hard drive will it remove a virus?', 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Numquam voluptatum nemo, similique, laboriosam earum minus excepturi aliquam placeat consectetur a sapiente culpa nihil quidem, nulla illo omnis ex architecto voluptatem.'),
(21, 1, 21, 'Access to files', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor'),
(22, 1, 22, 'Use of shared resources', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor'),
(23, 1, 23, 'Cross-platform', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor');

-- --------------------------------------------------------

--
-- Table structure for table `note_types`
--

CREATE TABLE `note_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `note_types`
--

INSERT INTO `note_types` (`id`, `name`) VALUES
(1, 'How it Works?'),
(2, 'A few reasons'),
(3, 'Reviews'),
(5, 'Questions'),
(6, 'TeamViewer Sidebar');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `type_id` tinyint(1) NOT NULL,
  `status_id` tinyint(1) NOT NULL,
  `rate_id` int(10) UNSIGNED DEFAULT NULL,
  `renewal` tinyint(1) NOT NULL DEFAULT '0',
  `payment_date` timestamp NULL DEFAULT NULL,
  `manager_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `done_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `client_id`, `type_id`, `status_id`, `rate_id`, `renewal`, `payment_date`, `manager_id`, `created_at`, `updated_at`, `done_at`) VALUES
(1, 1, 2, 2, NULL, 0, '2018-07-17 21:00:00', 1, '2018-07-17 21:00:00', '2018-07-17 21:00:00', NULL),
(2, 1, 1, 1, NULL, 0, '2018-07-30 21:00:00', NULL, '2018-07-30 21:00:00', '2018-08-06 07:48:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders_rates`
--

CREATE TABLE `orders_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `time_to_respond` tinyint(1) NOT NULL,
  `speed` tinyint(1) NOT NULL,
  `quality` tinyint(1) NOT NULL,
  `total_impression` tinyint(1) NOT NULL,
  `want_advise` tinyint(1) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders_statuses`
--

CREATE TABLE `orders_statuses` (
  `id` tinyint(1) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders_statuses`
--

INSERT INTO `orders_statuses` (`id`, `name`) VALUES
(1, 'waiting'),
(2, 'processing'),
(3, 'done'),
(4, 'undone');

-- --------------------------------------------------------

--
-- Table structure for table `orders_types`
--

CREATE TABLE `orders_types` (
  `id` tinyint(1) NOT NULL,
  `computers` int(10) UNSIGNED NOT NULL,
  `cost` double UNSIGNED NOT NULL,
  `period` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders_types`
--

INSERT INTO `orders_types` (`id`, `computers`, `cost`, `period`) VALUES
(1, 1, 5.99, 'one-time'),
(2, 1, 19.99, 'month'),
(3, 10, 99.99, 'month');

-- --------------------------------------------------------

--
-- Table structure for table `orders_type_lang`
--

CREATE TABLE `orders_type_lang` (
  `id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `order_type_id` tinyint(1) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders_type_lang`
--

INSERT INTO `orders_type_lang` (`id`, `language_id`, `order_type_id`, `name`, `text`) VALUES
(1, 1, 1, 'One-time', ''),
(2, 1, 2, 'month for one', ''),
(3, 1, 3, 'month for 10', ''),
(4, 2, 1, 'Одноразовая', ''),
(5, 2, 2, 'Месяц помощи на одном', ''),
(6, 2, 3, 'Месяц помощи на 10', '');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `ammount` float NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `payment_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `ammount`, `order_id`, `client_id`, `payment_date`) VALUES
(1, 19.99, 1, 1, '2018-07-17 21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'manager'),
(3, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `trial_ends_at` datetime DEFAULT NULL,
  `ends_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(1, 'animals');

-- --------------------------------------------------------

--
-- Table structure for table `tags_articles`
--

CREATE TABLE `tags_articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags_articles`
--

INSERT INTO `tags_articles` (`id`, `tag_id`, `article_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` tinyint(1) NOT NULL DEFAULT '3',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stripe_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role_id`, `photo`, `active`, `remember_token`, `created_at`, `updated_at`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`) VALUES
(1, 'Sozin', 'sozin1991@gmail.com', '$2y$10$.U.m55GOJIJEJ5GjXQ..xeV2JOujhTV3DBoS2L7pT4jMlpmc.PFpW', 2, '', 1, '23KHx7ujaIM8nPvqVFqG2r0bQ45XXpNcgBTchS4gCkW66idtGGCsHv5GvY0T', '2018-07-03 04:53:54', '2018-08-09 08:51:19', NULL, NULL, NULL, NULL),
(2, 'bob', 'bob@mail.ru', '33524', 2, '', 1, NULL, '2018-07-04 21:00:00', '2018-07-05 06:01:38', NULL, NULL, NULL, NULL),
(3, 'mike', 'mike@mail.ru', '$2y$10$KcGOzqX.ydbhzE2TnSCpmuyYtZBqBi6ALbTV7b6urS3rmZG1ijEWC', 3, NULL, 0, 'MvQbuArfH6sDOc4tfVmz85EfbrqJIw92JQLBklMTwhOpBzDf1GAP0jQsNrzR', '2018-07-06 10:21:10', '2018-07-06 10:21:10', NULL, NULL, NULL, NULL),
(4, 'Dmytro', 'small2004@ukr.net', '$2y$10$iPudJHHcFqPT8rilUCRswOXywkVLcleQerbv7C/q7R6wAdF6dH4FC', 3, NULL, 1, '9w94NyrdVyLMMHcl4TCPd1JIwdpu3QKLEh8GSTJ1kptl5kseHrLVHUaDIMex', '2018-08-07 10:27:50', '2018-08-09 05:26:17', NULL, NULL, NULL, NULL),
(29, 'Ирина Яновская', 'irma.yanovskaya.xz@gmail.com', '$2y$10$Yreualc8Jh866WyNXOYu3.sNE9WngIw2St587S1SWJmNdobJBfilq', 3, 'https://lh6.googleusercontent.com/-IFVCEW5da_4/AAAAAAAAAAI/AAAAAAAAAAA/AAnnY7qunYtv46cc1JyeV7FWTeBfjqX6pQ/mo/photo.jpg?sz=50', 1, 'nNjNdVBXPQgNoi5HSbPHfkq20GfHzy66FO9qzFctcfdc8kOu68okKWdHeG1C', '2018-08-08 05:57:00', '2018-08-08 05:57:00', NULL, NULL, NULL, NULL),
(30, 'Dmytro Bolychevskyi', 'dejibta@gmail.com', '$2y$10$5wfEpoh0E9KanA.LgU/I4OaP0LZK7hBaefkipYTAwO5cawcsg5/RC', 3, 'https://lh5.googleusercontent.com/-TivaD3XAi8I/AAAAAAAAAAI/AAAAAAAAAAA/AAnnY7qi0jzPJHEWVQzDATSadUdC2y5OjQ/mo/photo.jpg?sz=50', 1, '6xBQccDT9vsFQura8Zj8TpfuI0qRWCVVmTpGvvrk01eCXhb4htkj7C1MO7NC', '2018-08-08 06:44:00', '2018-08-09 09:22:21', NULL, NULL, NULL, NULL),
(35, 'Вячеслав Орлов', 'sozin33524@gmail.com', '$2y$10$popv5DJgwdmMapkfWv.jDegBDGR7iNZzFYcjYDJcHiiFTG.SbRQ6e', 3, 'https://lh5.googleusercontent.com/-9Dawmv_472s/AAAAAAAAAAI/AAAAAAAAAAA/AAnnY7rlANly4cuEYit37xe33AgPgFVEYg/mo/photo.jpg?sz=50', 1, 'JjI7LRiTFVPHny7X9XTpuqgCXZxLYHI2g3jSPYJArI7hJRbyQaqHWwvaB7RV', '2018-08-08 10:10:05', '2018-08-08 10:10:05', NULL, NULL, NULL, NULL),
(36, 'уйцуц', 'store@mail.ru', '$2y$10$7I4wIETE9EI0N9Ov3w.KT.Uf.6vWTzomAVcmbGzHMbgmJlvaIFyiu', 3, NULL, 0, NULL, '2018-08-09 05:25:29', '2018-08-09 05:25:29', NULL, NULL, NULL, NULL),
(37, 'Dmytro', 'dejibta2@gmail.com', '$2y$10$b5wPc.613GCqnQAO6v3RSevjqSRvGf8CgXmkZ53tc7dSZYslDQf4q', 3, NULL, 1, 'epOf0xg3TKZwxCPpPYpSSbzWITB6LowAg2ib0K2Ifymt4gHtstHg0YUFvfjk', '2018-08-09 05:26:43', '2018-08-09 05:28:36', NULL, NULL, NULL, NULL),
(38, 'Андрей', 'cequeditlabouchedombre@gmail.com', '$2y$10$CSwglbF8jdPN4p6p0.LVOe53RfmvYS.DcqLlY5CHr571OxZwEniCa', 3, NULL, 1, '7jj3DSV29lerKTtpFctDua1osOeagXtmpjPhMMtHQw6uZRUB7CNOVedSzrCb', '2018-08-09 05:49:43', '2018-08-09 05:50:19', NULL, NULL, NULL, NULL),
(39, 'qwe', 'qwe@qwd.qwe', '$2y$10$CfOqbmw8Xig9641RVBcRWOwh8crzrIkVDzOHI0pKVUqzo/BGudGeK', 3, NULL, 0, NULL, '2018-08-10 04:05:44', '2018-08-10 04:05:44', NULL, NULL, NULL, NULL),
(40, 'фыв', 'dejibta23@gmail.com', '$2y$10$lHXs2F103yB2uIBNjJwe0udY9HCyDZLDvnUqGEbVxjYFmcSo4QuX2', 3, NULL, 0, NULL, '2018-08-10 04:11:24', '2018-08-10 04:11:24', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`);

--
-- Indexes for table `blog_langs`
--
ALTER TABLE `blog_langs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `subject_id` (`blog_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `communication_channel_id` (`communication_channel_id`);

--
-- Indexes for table `communications`
--
ALTER TABLE `communications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communications_coordinates`
--
ALTER TABLE `communications_coordinates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `communication_channel_id` (`communication_channel_id`),
  ADD KEY `client_id` (`user_id`);

--
-- Indexes for table `computers`
--
ALTER TABLE `computers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `custom_fields`
--
ALTER TABLE `custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `handlings`
--
ALTER TABLE `handlings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `handlings_managers_comments`
--
ALTER TABLE `handlings_managers_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`handling_id`),
  ADD KEY `manager_id` (`manager_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `last_seen`
--
ALTER TABLE `last_seen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subject_type_id` (`subject_type_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `last_seen_types`
--
ALTER TABLE `last_seen_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `managers`
--
ALTER TABLE `managers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `menu_types`
--
ALTER TABLE `menu_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `navigation`
--
ALTER TABLE `navigation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_type_id` (`menu_type_id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `note_langs`
--
ALTER TABLE `note_langs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `note_types`
--
ALTER TABLE `note_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `order_type_id` (`type_id`),
  ADD KEY `order_status_id` (`status_id`),
  ADD KEY `order_rate_id` (`rate_id`),
  ADD KEY `manager_id` (`manager_id`);

--
-- Indexes for table `orders_rates`
--
ALTER TABLE `orders_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `orders_statuses`
--
ALTER TABLE `orders_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_types`
--
ALTER TABLE `orders_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_type_lang`
--
ALTER TABLE `orders_type_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_id` (`language_id`),
  ADD KEY `orders_type_id` (`order_type_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags_articles`
--
ALTER TABLE `tags_articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `article_id` (`article_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blog_langs`
--
ALTER TABLE `blog_langs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communications_coordinates`
--
ALTER TABLE `communications_coordinates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `computers`
--
ALTER TABLE `computers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `custom_fields`
--
ALTER TABLE `custom_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `handlings`
--
ALTER TABLE `handlings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `handlings_managers_comments`
--
ALTER TABLE `handlings_managers_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `last_seen`
--
ALTER TABLE `last_seen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `last_seen_types`
--
ALTER TABLE `last_seen_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `managers`
--
ALTER TABLE `managers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_types`
--
ALTER TABLE `menu_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `navigation`
--
ALTER TABLE `navigation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `note_langs`
--
ALTER TABLE `note_langs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `note_types`
--
ALTER TABLE `note_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders_rates`
--
ALTER TABLE `orders_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders_type_lang`
--
ALTER TABLE `orders_type_lang`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tags_articles`
--
ALTER TABLE `tags_articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `blog_langs`
--
ALTER TABLE `blog_langs`
  ADD CONSTRAINT `blog_langs_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`),
  ADD CONSTRAINT `blog_langs_ibfk_2` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`);

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `clients_ibfk_2` FOREIGN KEY (`communication_channel_id`) REFERENCES `communications` (`id`);

--
-- Constraints for table `communications_coordinates`
--
ALTER TABLE `communications_coordinates`
  ADD CONSTRAINT `communications_coordinates_ibfk_1` FOREIGN KEY (`communication_channel_id`) REFERENCES `communications` (`id`),
  ADD CONSTRAINT `communications_coordinates_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `computers`
--
ALTER TABLE `computers`
  ADD CONSTRAINT `computers_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `handlings`
--
ALTER TABLE `handlings`
  ADD CONSTRAINT `handlings_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `handlings_managers_comments`
--
ALTER TABLE `handlings_managers_comments`
  ADD CONSTRAINT `handlings_managers_comments_ibfk_1` FOREIGN KEY (`handling_id`) REFERENCES `handlings` (`id`),
  ADD CONSTRAINT `handlings_managers_comments_ibfk_2` FOREIGN KEY (`manager_id`) REFERENCES `managers` (`id`);

--
-- Constraints for table `last_seen`
--
ALTER TABLE `last_seen`
  ADD CONSTRAINT `last_seen_ibfk_1` FOREIGN KEY (`subject_type_id`) REFERENCES `last_seen_types` (`id`),
  ADD CONSTRAINT `last_seen_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `managers`
--
ALTER TABLE `managers`
  ADD CONSTRAINT `managers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `navigation`
--
ALTER TABLE `navigation`
  ADD CONSTRAINT `navigation_ibfk_1` FOREIGN KEY (`menu_type_id`) REFERENCES `menu_types` (`id`);

--
-- Constraints for table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `notes_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `note_types` (`id`);

--
-- Constraints for table `note_langs`
--
ALTER TABLE `note_langs`
  ADD CONSTRAINT `note_langs_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`),
  ADD CONSTRAINT `note_langs_ibfk_2` FOREIGN KEY (`note_id`) REFERENCES `notes` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`rate_id`) REFERENCES `orders_rates` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `orders_statuses` (`id`),
  ADD CONSTRAINT `orders_ibfk_4` FOREIGN KEY (`rate_id`) REFERENCES `orders_rates` (`id`),
  ADD CONSTRAINT `orders_ibfk_5` FOREIGN KEY (`type_id`) REFERENCES `orders_types` (`id`),
  ADD CONSTRAINT `orders_ibfk_6` FOREIGN KEY (`manager_id`) REFERENCES `managers` (`id`);

--
-- Constraints for table `orders_rates`
--
ALTER TABLE `orders_rates`
  ADD CONSTRAINT `orders_rates_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `orders_type_lang`
--
ALTER TABLE `orders_type_lang`
  ADD CONSTRAINT `orders_type_lang_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`),
  ADD CONSTRAINT `orders_type_lang_ibfk_2` FOREIGN KEY (`order_type_id`) REFERENCES `orders_types` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `payments_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `tags_articles`
--
ALTER TABLE `tags_articles`
  ADD CONSTRAINT `tags_articles_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`),
  ADD CONSTRAINT `tags_articles_ibfk_2` FOREIGN KEY (`article_id`) REFERENCES `blog` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
