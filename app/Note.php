<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    public function type()
    {
        return $this->belongsTo('App\Note_Type');
    }
    public function translation()
    {
        return $this->hasMany('App\note_lang','note_id');
    }
}
