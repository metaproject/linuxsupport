<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Handling_manager_comment extends Model
{
    protected $table = 'handlings_managers_comments';
    public function handling()
    {
        return $this->belongsTo('App\Handling');
    }
    public function manager()
    {
        return $this->belongsTo('App\Manager');
    }
    //
}
