<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communication_coordinate extends Model
{
    protected $table = 'communications_coordinates';
    protected $fillable =['communication_channel_id','user_id','coordinate'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function communication(){
        return $this->belongsTo('App\Communication','communication_channel_id');
    }
}
