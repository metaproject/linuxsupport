<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function order()
    {
        return $this->hasMany('App\Order');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function communication()
    {
        return $this->belongsTo('App\Communication','communication_channel_id');
    }
}
