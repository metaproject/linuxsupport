<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class note_lang extends Model
{
    public $timestamps = false;
    public function note()
    {
        return $this->belongsTo('App\Note');
    }
    public function lang()
    {
        return $this->belongsTo('App\Language');
    }
}
