<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Last_seen extends Model
{
    protected $table = 'last_seen';
    public $timestamps = false;
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function type()
    {
        return $this->belongsTo('App\Last_seen_type');
    }
    //
}
