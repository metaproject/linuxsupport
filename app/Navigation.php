<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Navigation extends Model
{
    public $timestamps = false;
    protected $table = 'navigation';
    public function menu_type()
    {
        return $this->belongsTo('App\Menu_type');
    }
}
