<?php
namespace App\SearchFactory;
use App;
class SearchDirector
{
    protected $methods;
    public function __construct($request)
    {
        if($request->isMethod('post')) {
            foreach ($request->sections as $value) {
                switch ($value) {
                    case 'orders':
                        $this->methods['orders'] = new OrderSearch;
                        break;
                    case 'clients':
                        $this->methods['clients'] = new ClientsSearch;
                        break;
                    case 'managers':
                        $this->methods['managers'] = new ManagersSearch;
                        break;
                    case 'blog':
                        $this->methods['blog'] = new BlogSearch;
                        break;
                }

            }
        }else{
            switch ($request->section) {
                case 'orders':
                    $this->methods['orders'] = new OrderSearch;
                    break;
                case 'clients':
                    $this->methods['clients'] = new ClientsSearch;
                    break;
                case 'managers':
                    $this->methods['managers'] = new ManagersSearch;
                    break;
                case 'blog':
                    $this->methods['blog'] = new BlogSearch;
                    break;
            }
        }
    }
    public function getMethods()
    {
        return $this->methods;
    }
}