<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.07.2018
 * Time: 10:45
 */

namespace App\SearchFactory;
use App;
use Validator;

class OrderSearch implements iSearch
{
    public function searching($request)
    {
        $this->searchValidator($request->all())->validate();
        $orders = App\Order::where('status_id','<',3)->with('order_status','order_type','client.communication','client.user.phone','client.user.communication.communication','handling','manager.user');
        if ($request->name) {
            $orders= $orders->whereHas('client.user', function ($query) use ($request){
                $query->where('name', 'like', '%'.$request->name.'%');
            });
        }
        if ($request->phone) {
            $orders= $orders->whereHas('client.user.phone', function ($query)use ($request) {
                $query->where('coordinate', 'like',$request->phone);
            });
        }
        if ($request->pay_date_first) {
            $orders= $orders->whereBetween('payment_date',[$request->pay_date_first,$request->pay_date_last]);
        }
        if ($request->order) {
            $orders= $orders->where('id',$request->order);
        }
        if ($request->status) {
            $orders= $orders->where('status_id',$request->status);
        }
        if ($request->manager) {
            $orders= $orders->whereHas('manager', function ($query) use ($request){
                $query->where('id', $request->manager);
            });
        }
        $orders=$orders->orderBy('created_at', 'desc')->pluck('id')->toArray();
        return $orders;
    }
    /*protected function OrderExpireDate($collection) {
        $collection->map(function ($item){
            return $item->expire_date=App\Order::find($item->id)->payment_date->addMonths(1)->format('Y-m-d H:i:s');
        });
        return $collection;
    }*/
    protected function searchValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required_without_all:phone,pay_date_first,order,status,manager',
            'phone'=>'required_without_all:name,pay_date_first,order,status,manager',
            'pay_date_first'=>'required_without_all:name,phone,order,status,manager|required_with:pay_date_last',
            'pay_date_last'=>'required_without_all:name,phone,order,status,manager|required_with:pay_date_first',
            'order'=>'required_without_all:name,phone,pay_date_first,status,manager',
            'status'=>'required_without_all:name,phone,pay_date_first,order,manager',
            'manager'=>'required_without_all:name,phone,pay_date_first,order,status',
        ]);
    }
}