<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.07.2018
 * Time: 10:51
 */

namespace App\SearchFactory;
use App;
use Validator;

class ManagersSearch implements iSearch
{
 public function searching($request)
 {
     $this->searchValidator($request->all())->validate();
     $managers = App\Manager::with('user','management','user.communication','user.communication.communication');
     if ($request->name) {
         $managers= $managers->where('name', 'like', '%'.$request->name.'%');
     }
     if ($request->phone) {
         $managers= $managers->whereHas('communication', function ($query)use ($request) {
             $query->where('communication_channel_id',  6)->where('coordinate', 'like',$request->phone);
         });
     }
     if ($request->pay_date) {
         $managers= $managers->whereHas('management', function ($query)use ($request) {
             $query->where('payment_date',  $request->pay_date);
         });
     }
     if ($request->order) {
         $managers= $managers->whereHas('management', function ($query)use ($request) {
             $query->where('id',  $request->order);
         });
     }
     if ($request->status) {
         $managers= $managers->whereHas('management', function ($query)use ($request) {
             $query->where('status_id',  $request->status);
         });
     }
     $managers=$managers->paginate(10);
     return $managers;
 }
    protected function searchValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required_without_all:phone,pay_date,order,status,manager',
            'phone'=>'required_without_all:name,pay_date,order,status,manager',
            'pay_date'=>'required_without_all:name,phone,order,status,manager',
            'order'=>'required_without_all:name,phone,pay_date,status,manager',
            'status'=>'required_without_all:name,phone,pay_date,order,manager',
        ]);
    }
}