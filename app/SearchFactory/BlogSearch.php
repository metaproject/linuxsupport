<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.07.2018
 * Time: 11:29
 */

namespace App\SearchFactory;
use App;

class BlogSearch implements iSearch
{
    protected $lang;
    public function __construct()
    {
        $this->lang = App\Language::where('name',App::getLocale())->first();
    }

    public function searching($request)
    {
            $articles = App\Blog::with('translation')->whereHas('translation', function ($q) use ($request) {
                $q->where('language_id', $this->lang->id);
                if ($request->string!='all') {
                    $q->where('text', 'like', "%{$request->string}%")
                        ->orWhere('title', 'like', "%{$request->string}%");
                }

            })->orderBy('created_at', 'desc')->pluck('id')->toArray();

        return $articles;
    }
}