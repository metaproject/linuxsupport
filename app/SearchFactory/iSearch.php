<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.07.2018
 * Time: 10:42
 */

namespace App\SearchFactory;


interface iSearch
{

    public function searching($request);
}