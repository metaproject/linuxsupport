<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.07.2018
 * Time: 10:49
 */

namespace App\SearchFactory;
use App;
use Validator;

class ClientsSearch implements iSearch
{
    public function searching($request)
    {
        $this->searchValidator($request->all())->validate();
        $clients = App\Client::with('user.communication','communication');
        if ($request->name) {
            $clients= $clients->whereHas('user', function ($query) use ($request){
                $query->where('name', 'like', '%'.$request->name.'%');
            });
        }
        if ($request->phone) {
            $clients= $clients->whereHas('user.communication', function ($query)use ($request) {
                $query->where('communication_channel_id',  6)->where('coordinate', 'like',$request->phone);
            });
        }
        if ($request->pay_date) {
            $clients= $clients->whereHas('order', function ($query)use ($request) {
                $query->where('payment_date',  $request->pay_date);
            });
        }
        if ($request->order) {
            $clients= $clients->whereHas('order', function ($query)use ($request) {
                $query->where('id',  $request->order);
            });
        }
        if ($request->status) {
            $clients= $clients->whereHas('order', function ($query)use ($request) {
                $query->where('status_id',  $request->status);
            });
        }
        $clients=$clients->paginate(10);
        return $this->ClientPreferCoordinate($clients);
    }
    protected function ClientPreferCoordinate($collection) {
        $collection->map(function ($item){
            return $item->preferCoordinate=App\Communication_coordinate::where('user_id',$item->user_id)->where('communication_channel_id',$item->communication_channel_id)->first()->coordinate;
        });
        return $collection;
    }
    protected function searchValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required_without_all:phone,pay_date,order,status,manager',
            'phone'=>'required_without_all:name,pay_date,order,status,manager',
            'pay_date'=>'required_without_all:name,phone,order,status,manager',
            'order'=>'required_without_all:name,phone,pay_date,status,manager',
            'status'=>'required_without_all:name,phone,pay_date,order,manager',
        ]);
    }
}