<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order_type extends Model
{
    protected $table = 'orders_types';
    public $timestamps = false;
    public function order()
    {
        return $this->hasMany('App\Order','type_id');
    }
    public function translation(){
        return $this->hasMany('App\Order_type_lang');
    }
    public function getPeriodLengthAttribute()
    {
        if($this->period=='one-time') {
            return 1;
        }else{
            $payment_date=$this->order->first()->payment_date;
            return $payment_date->addMonth(1)->diffinDays($this->order->first()->payment_date);
        }
    }
}
