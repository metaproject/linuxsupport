<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;
class Blog_lang extends Model
{
    use Eloquence;

    // no need for this, but you can define default searchable columns:
    protected $searchableColumns = ['title', 'text'];
    public $timestamps = false;
    public function blog()
    {
        return $this->belongsTo('App\Blog');
    }
    public function lang()
    {
        return $this->belongsTo('App\Language');
    }
}
