<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public function blog_lang()
    {
        return $this->hasMany('App\Blog_lang');
    }
    public function note_lang()
    {
        return $this->hasMany('App\note_lang');
    }
}
