<?php namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Если композер реализуется при помощи класса:
        View::composer('layouts.app', 'App\Http\ViewComposers\ProfileComposer');
        View::composer('blog.layouts.app', 'App\Http\ViewComposers\ProfileComposer');
        View::composer('admin.layouts.admin', 'App\Http\ViewComposers\AdminMenuComposer');
    }

    /**
     * Register
     *
     * @return void
     */
    public function register()
    {
        //
    }

}