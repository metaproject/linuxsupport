<?php
/**
 * Ulogin.ru auto registration or login.
 */
namespace App\GlobalTraits;
use App;
use Auth;
use Illuminate\Http\Request;
use App\SearchFactory;

trait SetSession
{
    public function orderPay(Request $request){
        session(['order' => $request->post()]);
        session(['redirected' => 0]);
        if(Auth::check()&&Auth::user()->active==1) {
            $resp=true;
        }else{
            $resp=false;
        }
        return response()->json(['active'=>$resp]);
    }
    public function changePlanSession($prefer,$phone,$cost,$type){
        $resp=['client'=>['phone'=>$phone->coordinate,'prefer'=>$prefer],'order'=>['type'=>$type,'cost'=>$cost]];
        session(['order' => $resp]);
            session(['redirected' => 0]);
    }
}