<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    public function user()
    {
        return $this->belongsTo('App\user');
    }
    public function management()
    {
        return $this->hasMany('App\Order','manager_id');
    }
    public function managementCount()
    {
        return $this->hasMany('App\Order','manager_id')->where('status_id',3)->whereMonth('done_at', '=', date('m'))->selectRaw('manager_id, count(*) as count')->groupBy('manager_id');
    }
}
