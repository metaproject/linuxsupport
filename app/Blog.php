<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog';
    public function user()
    {
        return $this->belongsTo('App\User','author_id');
    }
    public function tags()
    {
        return $this->belongsToMany('App\Tag','tags_articles','article_id');
    }
    public function translation()
    {
        return $this->hasMany('App\Blog_lang','blog_id');
    }
}
