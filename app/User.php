<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Billable;
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function role()
    {
        return $this->belongsTo('App\Role');
    }
    public function client()
    {
        return $this->hasOne('App\Client');
    }
    public function communication()
    {
        return $this->hasMany('App\Communication_coordinate');
    }
    public function phone()
    {
        return $this->hasMany('App\Communication_coordinate')->where('communication_channel_id',6);
    }
    public function inGroup($groupName) {
        return $this->role->name==$groupName;
    }

    public function articles()
    {
        return $this->hasMany('App\Blog','author_id');
    }
    public function manager()
    {
        return $this->hasOne('App\Manager');
    }
}
