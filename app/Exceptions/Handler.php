<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Exceptions;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \app\Exceptions\AuthenticationAdminException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception){

        if($exception instanceof Exceptions\AuthenticationAdminException){
            if ($request->expectsJson()) {
                return response()->json(['error' => 'Unauthenticated.'], 401);
            }
            return redirect()->guest('/admin/login');
        }
        if($exception instanceof AuthenticationException){
            if ($request->isMethod('post')) {
                $request->session()->put('request', $request->all());
            }
        }
        if($exception instanceof ActivatedUserException){
            \Session::flash('flash_message_error', trans('messages.ActivatePlease1').'<a href="'.route('newActivateMess').'">'.trans('messages.ActivatePlease2').'</a>.');
            return redirect()->route('home');
        }
        return parent::render($request, $exception);
    }
}
