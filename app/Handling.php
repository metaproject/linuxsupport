<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Handling extends Model
{
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    public function manager_comments()
    {
        return $this->hasMany('App\Order_manager_comment');
    }
}
