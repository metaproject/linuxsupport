<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communication extends Model
{
    public $timestamps = false;
    public function coordinate(){
        return $this->hasMany('App\Communication_coordinate');
    }
    public function client()
    {
        return $this->hasMany('App\Client');
    }
}
