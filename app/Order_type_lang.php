<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_type_lang extends Model
{
    protected $table = 'orders_type_lang';
    public $timestamps = false;
    public function Order_type()
    {
        return $this->belongsTo('App\Order_type','order_type_id');
    }
}