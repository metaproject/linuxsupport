<?php
namespace App\PaymentMethodsFactory;
use App;
use Auth;

class PaymentDirector
{
    protected $payment;
    public function __construct($method)
    {
            switch ($method) {
                case 'Stripe':
                    $this->payment = new Stripe;
                    break;
                case 'PayPal':
                    $this->payment = new PayPal;
                    break;
                case 'Webmoney':
                    $this->payment = new Webmoney;
                    break;
                case 'Yandex':
                    $this->payment = new Yandex;
                    break;
            }
    }
    public function getPayment()
    {
        return $this->payment;
    }
}