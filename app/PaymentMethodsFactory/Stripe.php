<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.07.2018
 * Time: 11:29
 */

namespace App\PaymentMethodsFactory;
use App;
use Validator;
use Illuminate\Http\Request;

class Stripe implements iPay
{
    public function payment(Request $request, array $order,$amount)
    {
        $input=$request->all();
        $this->PaymentValidator($request->all())->validate();
            $input = array_except($input,array('_token'));
            $stripe = \Stripe::make('sk_test_lIgBhfpzWDnlisuUYkMB3JBP');
            try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number'    => $request->get('card_number'),
                        'exp_month' => $request->get('exp_month'),
                        'exp_year'  => $request->get('exp_year'),
                        'cvc'       => $request->get('cvv'),
                    ],
                ]);
                if (!isset($token['id'])) {
                    return response()->json(['error' => 'The Stripe Token was not generated correctly']);
                }
                $charge = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => 'USD',
                    'amount'   => $amount,
                    'description' => $order['type'],
                ]);
                if($charge['status'] == 'succeeded') {
                    return true;
                } else {
                    return response()->json(['error' => 'Money not add in wallet!!']);
                }
            } catch (Exception $e) {
                \Session::put('error',$e->getMessage());
                return false;
            } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
                \Session::put('error',$e->getMessage());
                return false;
            } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                \Session::put('error',$e->getMessage());
                return false;
            }
    }
    protected function PaymentValidator(array $data){
        return Validator::make($data, [
            'card_number'=>'required',
            'cvv'=>'required',
            'exp_month'=>'required',
            'exp_year'=>'required',
        ]);

    }
}