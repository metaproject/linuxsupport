<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.07.2018
 * Time: 10:42
 */

namespace App\PaymentMethodsFactory;
use Illuminate\Http\Request;

interface iPay
{
    public function payment(Request $request,array $order,$amount);
}