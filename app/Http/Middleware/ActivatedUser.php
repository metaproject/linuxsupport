<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use App\Exceptions;


class ActivatedUser
{
    public function handle($request, Closure $next)
    {
        if(Auth::user()->active==1) {
                    return $next($request);
        }

        throw new Exceptions\ActivatedUserException('Unactivated.');
    }
}
