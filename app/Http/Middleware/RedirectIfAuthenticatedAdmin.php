<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticatedAdmin
{
    public function handle($request, Closure $next, ...$groupName)
    {
        if(Auth::check()) {
            foreach ($groupName as $value) {
                if (Auth::user()->inGroup($value)) {
                    return redirect('/admin');
                }
            }
        }

        return $next($request);
    }
}
