<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use App\Exceptions;

class Search
{
    protected $guestSections=['blogs'];
    protected $managerSections=['managers','blogs','orders','clients'];
    public function handle($request, Closure $next)
    {
                if (!Auth::user()->inGroup('manager')&&!Auth::user()->inGroup('admin')) {
                    $request->sections=array_intersect($request->sections,$this->$guestSections);
                }
        return $next($request);
    }
}
