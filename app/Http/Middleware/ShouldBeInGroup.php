<?php

namespace App\Http\Middleware;
use Auth;
use Closure;
use App\Exceptions;
use View;

class ShouldBeInGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$groupName)
    {
        if(Auth::check()) {
            foreach ($groupName as $value) {
                if (Auth::user()->inGroup($value)) {
                    View::share(['isadmin' => Auth::user()->inGroup('admin')]);
                    return $next($request);
                }
            }
        }

        throw new Exceptions\AuthenticationAdminException('Unauthenticated.');
    }
}
