<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container\Container;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Middleware\SuperAdminMiddleware;
use \App\Http\Controllers;

class AdminRoles
{
    private static $ROLES = [
        'manager' => 'Manager',
        'admin' => 'Admin'
    ];

    public function handle(Request $request, Closure $next)
    {
            //change action
            $action = $request->route()->getAction();
            $sub = self::$ROLES[$request->user()->role->name];
            $uses = explode('\\', $action['uses']);
            $controller = array_pop($uses);
            $newcontroller = $sub . $controller;
            array_push($uses, $newcontroller);
            $action['uses'] = implode('\\', $uses);
            $action['controller'] = str_replace($controller, $newcontroller, $action['controller']);
            $request->route()->setAction($action);
            //change controller
            $class=explode('@', $newcontroller)[0];
            $class='\\App\\Http\\Controllers\\Admin\\'.$class;
            $controller = app($class);

            $request->route()->controller = $controller;

        return $next($request);
    }
}
