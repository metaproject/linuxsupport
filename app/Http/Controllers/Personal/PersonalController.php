<?php

namespace App\Http\Controllers\Personal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
use Auth;
use Validator;
use Hash;

class PersonalController extends Controller
{
    public function index(){
        $lang = collect([App\Language::where('name',App::getLocale())->first(),App\Language::where('name','!=',App::getLocale())->first()]);

        $orders=App\Order::where('client_id',1)->with('client','client.user','handling','order_type.translation')->whereHas('order_type.translation',function ($q)use($lang){
            $q->where('language_id', $lang->first())->orWhere('language_id', 1);
        })->whereHas('client',function ($q){
            $q->where('user_id',Auth::user()->id);
            $q->where('user_id',Auth::user()->id);
        })->orderBy('created_at','desc')->get();
        $orders_types=App\Order_type::with('translation')->get();
        $user=App\User::where('id',Auth::user()->id)->with('communication.communication')->first();

        return view('personal.personal',compact('orders','lang','orders_types','user'));
    }
    public function changePersonalData(Request $request){
        $this->changeValidator($request->all())->validate();
        $user=App\User::with('communication.communication')->find(Auth::user()->id);
        $changed=array();
        if($request->phone) {
            $phone = App\Communication_coordinate::firstorNew(['communication_channel_id' => 6, 'user_id' => Auth::user()->id]);
            $phone->coordinate = $request->phone;
            $phone->save();
            $changed[]='phone';
        }
        if($request->name) {
            $user->name=$request->name;
            $changed[]='name';
        }
        if($request->passOld)
        {
            if(Hash::check($request->passOld, Auth::User()->password)){
                $user->password = Hash::make($request->password);
                $changed[]='password';
            }
        }
        if($request->email)
        {
                $user->email = $request->email;
                if($email_coord=App\Communication_coordinate::where('communication_channel_id', 5)->where('user_id',Auth::user()->id)->first()) {
                    $email_coord->coordinate = $request->email;
                    $email_coord->save();
                }
            $changed[]='email';
        }
        $user->save();
        return response()->json(['success' => 'All changed succesfuly',$changed]);
    }
    protected function changeValidator(array $data)
    {
        return Validator::make($data, [
            'phone'=>'string',
            'name'=>'string',
            'email'=>'string|confirmed',
            'password'=>'required_with:passOld|string|min:6|confirmed',
            'passOld'=>'required_with:password|string'
        ]);
    }
}
