<?php

namespace App\Http\Controllers\Inform;
use App\Http\Controllers;
use App;
use Illuminate\Http\Request;

class InformController extends Controllers\Controller
{
    public function index()
    {
        $lang = collect([App\Language::where('name',App::getLocale())->first(),App\Language::where('name','!=',App::getLocale())->first()]);
        $notes=App\Note::where('type_id',6)->with('translation','type')->whereHas('translation',function($q) use ($lang)
        {
            $q->where('language_id', $lang->first())->orWhere('language_id', '=', 1);

        })->get();
        return view('inform.inform',compact('lang','notes'));
    }
}
