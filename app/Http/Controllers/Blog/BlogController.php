<?php

namespace App\Http\Controllers\Blog;
use App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Traits;

class BlogController extends Controllers\Controller
{
    use Traits\SearchTrait;
    public function index(Request $request)
    {
        $lang = collect([App\Language::where('name',App::getLocale())->first(),App\Language::where('name','!=',App::getLocale())->first()]);
        if($request->search)
        {
            $articles=App\Blog::whereIn('id', json_decode($request->search))->with('translation')->whereHas('translation',function($q) use ($lang,$request)
            {
                $q->where('language_id', $lang->first())->orWhere('language_id', '=', 1);
            })->paginate(9);
        }else{
            $articles=App\Blog::with('translation')->whereHas('translation',function($q) use ($lang,$request)
            {
                $q->where('language_id', $lang->first())->orWhere('language_id', '=', 1);
            })->paginate(9);
        }


        return view('blog.blog',compact('articles','lang'));
    }
    public function article($article){
        $lang = collect([App\Language::where('name',App::getLocale())->first(),App\Language::where('name','!=',App::getLocale())->first()]);

        $article=App\Blog::where('id',$article)->with('translation','tags')->whereHas('translation',function($q) use ($lang)
        {
            $q->where('language_id', $lang->first())->orWhere('language_id', '=', 1);
        })->first();
        $articles=App\Blog::where('recomended',1)->where('id','!=',$article->id)->with('translation')->whereHas('translation',function($q) use ($lang)
        {
            $q->where('language_id', $lang->first())->orWhere('language_id', '=', 1);
        })->take(5)->get();
        return view('blog.post',compact('articles','article','lang'));
    }
}
