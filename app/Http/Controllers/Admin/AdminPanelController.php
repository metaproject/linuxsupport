<?php

namespace App\Http\Controllers\Admin;
use App;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;


class AdminPanelController extends PanelController
{
    use Traits\OrdersAdminTrait,Traits\ManagersAdminTrait,Traits\HandlingsAdminTrait,Traits\FieldsAdminTrait,Traits\BlogAdminTrait,Traits\NotesAdminTrait;
    //controller for admin`s speciffic pages
    public function __construct(){
        $this->middleware('inGroup:admin');
    }
    public function index()
    {
        return view('admin.home');
    }
}
