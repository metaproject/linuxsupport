<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class AdminAuthController extends Controller
{
    public function getLogin()
    {
        $title = 'Entrance';
        return view('admin.login',['title'=>$title]);
    }
    public function postLogin(Request $request)
    {
        if (App\User::where('email', $request->email)->where(function ($q) {
            $q->where('role_id', 2)
                ->orWhere('role_id', 1);
        })->first()
        ) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                // success
                return redirect()->route('adminOrders');
            }

        }
        return redirect()->route('adminAuth')->withErrors(['wrongAuth'=>'Wrong Email or Password']);
    }

}
