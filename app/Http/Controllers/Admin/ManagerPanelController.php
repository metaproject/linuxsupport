<?php

namespace App\Http\Controllers\Admin;
use App;
use Illuminate\Http\Request;

class ManagerPanelController extends PanelController
{
    use Traits\OrdersManagerTrait;
    //controller for manager`s speciffic pages
    public function __construct(){
        $this->middleware('inGroup:manager,admin');
    }
    public function index()
    {
        return view('admin.home');
    }
}
