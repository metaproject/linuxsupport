<?php

namespace App\Http\Controllers\Admin\Traits;
use App;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

trait BlogAdminTrait
{
    public function addArticle(Request $request)
    {
        $this->addBlogValidator($request->all())->validate();
        $article = new App\Blog;
        //download image
        if ($request->photo) {
            if ($content = file_get_contents($request->photo)) {
                $arr_url = explode('/', $request->photo);
                $name = uniqid() . array_pop($arr_url);
                if (Storage::disk('public')->put($name, $content)) {
                    $article->photo = Storage::url($name);
                }
            }
        }
        //add translation
        if(count($request->translations)>0) {
            foreach ($request->translations as $key => $value) {
                $translation=new App\Blog_lang;
                $translation->title=$value->title;
                $translation->text=$value->text;
                $translation->language_id=$key;
                $translation->save();
            }
        }
        if ($article->save()) {
            //return new list
            $articles = App\Blog::with('user', 'tags')->orderBy('created_at', 'desc')->paginate(10);
            return response()->json(['success' => 'Article added successfully', $articles]);
        }

    }

    public function updateArticle(Request $request)
    {
        $this->addBlogValidator($request->all())->validate();
        //download image
        $article = App\Blog::find($request->id);
        if ($request->photo) {
            if ($content = file_get_contents($request->photo)) {
                $arr_url = explode('/', $request->photo);
                $name = uniqid() . array_pop($arr_url);
                if (Storage::disk('public')->put($name, $content)) {
                    $article->photo = Storage::url($name);
                }
            }
        }
        //update translation
        if (count($request->translations) > 0) {
            foreach ($request->translations as $key => $value) {
                if (!$translation = App\Blog_lang::where('subject_id', $request->id)->where('language_id', $key)->first()) {
                    $translation = new App\Blog_lang;
                    $translation->language_id = $key;
                }
                $translation->title = $value->title;
                $translation->text = $value->text;
                $translation->save();
            }

        }
        if ($article->save()) {
            //return new list
            $articles = App\Blog::with('user', 'tags','Blog_lang')->orderBy('created_at', 'desc')->paginate(10);
            return response()->json(['success' => 'Article updated successfully', $articles]);
        }

    }
    public function deleteArticle(Request $request){
        $this->deleteArticleValidator($request->all())->validate();
        $article = App\Blog::find($request->id)->delete();
        if($article) {
            App\Blog_lang::where('subject_id',$request->id)->all()->delete();
            $articles = App\Blog::with('user', 'tags','Blog_lang')->orderBy('created_at', 'desc')->paginate(10);
                return response()->json(['success'=>'Manager deleted successfully',$articles]);
        }else{
            return response()->json(['errors'=>'User can`t be deleted']);
        }
    }
    protected function addBlogValidator(array $data)
    {
        return Validator::make($data, [
            'translations.*.title'=>'required|string|min:5|max:20',
            'translations.*.text'=>'required|string|min:10',
            'translations.*'=>'required|int|exists:language,id',
            'photo' => 'url|mimes:jpg,png|string|min:5|max:255'
        ]);
    }
    protected function deleteArticleValidator(array $data)
    {
        return Validator::make($data, [
            'id'=>'required|int'
        ]);
    }
}
