<?php

namespace App\Http\Controllers\Admin\Traits;
use App;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

trait CommentManagerTrait
{
    public function addComment(Request $request)
    {
        $this->addCommentValidator($request->all())->validate();
        $comment = new App\Handling_manager_comment();
        //add comment
        $comment->handling_id = $request->handling_id;
        $comment->manager_id = $request->manager_id;
        $comment->comment = $request->comment;
        if ($comment->save()) {
            //return new list
            /*$notes=App\Note::with('Note_lang')->orderBy('created_at','desc')->paginate(10);
            return response()->json(['success' => 'Note added successfully', 'notes'=>$notes]);*/
        }

    }

    public function updateComment(Request $request)
    {
        $this->updateCommentValidator($request->all())->validate();
        $comment = App\Handling_manager_comment()::where('id',$request->comment_id);
        //add comment
        $comment->comment = $request->comment;
        if ($comment->save()) {
            //return new list
            /*$notes=App\Note::with('Note_lang')->orderBy('created_at','desc')->paginate(10);
            return response()->json(['success' => 'Note added successfully', 'notes'=>$notes]);*/
        }

    }
    protected function addCommentValidator(array $data)
    {
        return Validator::make($data, [
            'handling_id'=>'required|int|exists:handlings,id',
            'manager_id'=>'required|int|exists:managers,id',
            'comment'=>'required|string|min:3'
        ]);
    }
    protected function updateCommentValidator(array $data)
    {
        return Validator::make($data, [
            'id'=>'required|int|exists:handlings_managers_comments,id',
            'comment'=>'required|string|min:3'
        ]);
    }
}
