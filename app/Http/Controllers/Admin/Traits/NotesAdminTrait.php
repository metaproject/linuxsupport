<?php

namespace App\Http\Controllers\Admin\Traits;
use App;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

trait NotesAdminTrait
{
    public function addNote(Request $request)
    {
        $this->addNoteValidator($request->all())->validate();
        $note = new App\Note;
        //download image
        if ($request->image) {
            if ($content = file_get_contents($request->image)) {
                $arr_url = explode('/', $request->image);
                $name = uniqid() . array_pop($arr_url);
                if (Storage::disk('public')->put($name, $content)) {
                    $note->image = Storage::url($name);
                }
            }
        }
        //add translation
        if(count($request->translations)>0) {
            foreach ($request->translations as $key => $value) {
                $translation=new App\Note_lang;
                $translation->title=$value->title;
                $translation->text=$value->text;
                $translation->language_id=$key;
                $translation->save();
            }
        }
        //add note
        $note->type_id = $request->type_id;
        if ($note->save()) {
            //return new list
            $notes=App\Note::with('Note_lang')->orderBy('created_at','desc')->paginate(10);
            return response()->json(['success' => 'Note added successfully', 'notes'=>$notes]);
        }

    }

    public function updateNote(Request $request)
    {
        $this->addNoteValidator($request->all())->validate();
        //download image
        $note = App\Note::find($request->id);
        if ($request->image) {
            if ($content = file_get_contents($request->image)) {
                $arr_url = explode('/', $request->image);
                $name = uniqid() . array_pop($arr_url);
                if (Storage::disk('public')->put($name, $content)) {
                    $note->image = Storage::url($name);
                }
            }
        }
        //update translation
        if (count($request->translations) > 0) {
            foreach ($request->translations as $key => $value) {
                if (!$translation = App\Note_lang::where('subject_id', $request->id)->where('language_id', $key)->first()) {
                    $translation = new App\Note_lang;
                    $translation->language_id = $key;
                }
                $translation->title = $value->title;
                $translation->text = $value->text;
                $translation->save();
            }

        }
        //update note
        if($request->type_id)
            $note->type_id = $request->type_id;
        if ($note->save()) {
            //return new list
            $notes=App\Note::with('Note_lang')->orderBy('created_at','desc')->paginate(10);
            return response()->json(['success' => 'Note updated successfully', 'notes'=>$notes]);
        }

    }
    public function deleteNote(Request $request){
        $this->deleteNoteValidator($request->all())->validate();
        $note = App\Note::find($request->id)->delete();

        if($note) {
            App\Translation::where('subject_id',$request->id)->delete();
            $notes=App\Note::orderBy('created_at','desc','Note_lang')->paginate(10);
            return response()->json(['success' => 'Note deleted successfully', 'notes'=>$notes]);
        }else{
            return response()->json(['errors'=>'Note can`t be deleted']);
        }
    }
    public function addNoteType(Request $request)
    {
        $this->addNoteTypeValidator($request->all())->validate();
        $note_type = new App\Note_Types;
        //add note_type
        $note_type->name = $request->name;
        if ($note_type->save()) {
            //return new list
            $note_types=App\Note_Type::all();
            return response()->json(['success' => 'Note_Type added successfully','note_types'=>$note_types]);
        }

    }
    public function deleteNoteType(Request $request){
        $this->deleteNoteValidator($request->all())->validate();
        $note_type = App\Note_Type::find($request->id)->delete();
        if($note_type) {
            $note_types=App\Note_Type::all();
            return response()->json(['success' => 'Note deleted successfully','note_types'=>$note_types]);
        }else{
            return response()->json(['errors'=>'Note can`t be deleted']);
        }
    }
    protected function addNoteValidator(array $data)
    {
        return Validator::make($data, [
            'translations.*.title'=>'required|string|min:5|max:20',
            'type_id'=>'int|exists:note_types,id',
            'translations.*.text'=>'required|string|min:10',
            'translations.*'=>'required|int|exists:languages,id',
            'image' => 'url|mimes:jpg,png|string|min:5|max:255'
        ]);
    }
    protected function deleteNoteValidator(array $data)
    {
        return Validator::make($data, [
            'id'=>'required|int'
        ]);
    }
    protected function addNoteTypeValidator(array $data)
    {
        return Validator::make($data, [
            'name'=>'required|string|min:5|max:20'
        ]);
    }
}
