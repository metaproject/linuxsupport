<?php

namespace App\Http\Controllers\Admin\Traits;
use App\Http\Controllers;
use Illuminate\Http\Request;
use App;

trait FieldsAdminTrait
{
    public function addField(Request $request)
    {
        $this->addFieldValidator($request->all())->validate();
        $field = new App\Custom_Field;
        //add field
        $field->alias = $request->alias;
        $field->value = $request->value;
        if ($field->save()) {
            //return new list
            $fields=App\Custom_Field::paginate(10);
            return response()->json(['success' => 'Field added successfully', 'fields'=>$fields]);
        }

    }

    public function updateField(Request $request)
    {
        $this->addFieldValidator($request->all())->validate();
        $field = App\Custom_Field::find($request->id);

        //update field
            $field->alias = $request->alias;
            $field->value = $request->value;
        if ($field->save()) {
            //return new list
            $fields=App\Custom_Field::paginate(10);
            return response()->json(['success' => 'Field updated successfully', 'fields'=>$fields]);
        }

    }
    public function deleteField(Request $request){
        $this->deleteFieldValidator($request->all())->validate();
        $field = App\Custom_Field::find($request->id)->delete();

        if($field) {
            $fields=App\Custom_Field::paginate(10);
            return response()->json(['success' => 'Field deleted successfully', 'fields'=>$fields]);
        }else{
            return response()->json(['errors'=>'Field can`t be deleted']);
        }
    }
    protected function addFieldValidator(array $data)
    {
        return Validator::make($data, [
            'value'=>'required|string|min:3|max:20',
            'alias'=>'required|string|min:3|max:20',
        ]);
    }
    protected function deleteFieldValidator(array $data)
    {
        return Validator::make($data, [
            'id'=>'required|int'
        ]);
    }
}
