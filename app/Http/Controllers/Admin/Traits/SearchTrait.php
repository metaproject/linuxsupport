<?php

namespace App\Http\Controllers\Admin\Traits;
use App;
use App\Http\Controllers;
use Illuminate\Http\Request;
use App\SearchFactory;

trait SearchTrait
{
    protected $search;
    public function search(Request $request){
        $director=new SearchFactory\SearchDirector($request);
        $this->search=$director->getMethods();
        $respond=array();
        foreach ($this->search as $section=>$method){
            $result=$method->searching($request);
            if($request->isMethod('get')){
                return $result;
            }
                $respond[$section]=$result;
        }
            return response()->json($respond);

    }
}
