<?php

namespace App\Http\Controllers\Admin\Traits;
use App;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

trait HandlingsAdminTrait
{
    public function updateHandling(Request $request)
    {
        $this->updateHandlingValidator($request->all())->validate();
        $handling = App\Handling::find($request->id);
        //update Handling
        if($request->problem)
            $handling->problem = $request->problem;
        if($request->done||$request->done===false)
            $handling->done = $request->done;
        if ($handling->save()) {
            //return new list
            $handlings=App\Handling::orderBy('created_at','desc')->paginate(10);
            return response()->json(['success' => 'Handling updated successfully', 'handlings'=>$handlings]);
        }

    }
    public function deleteHandling(Request $request){
        $this->deleteHandlingValidator($request->all())->validate();
        $handling = App\Handling::find($request->id)->delete();

        if($handling) {
            $handlings=App\Handling::orderBy('created_at','desc')->paginate(10);
            return response()->json(['success' => 'Handling deleted successfully', 'handlings'=>$handlings]);
        }else{
            return response()->json(['errors'=>'Handling can`t be deleted']);
        }
    }
    protected function updateHandlingValidator(array $data)
    {
        return Validator::make($data, [
            'problem'=>'string|max:500',
            'done'=>'boolean',
        ]);
    }
    protected function deleteHandlingValidator(array $data)
    {
        return Validator::make($data, [
            'id'=>'required|int'
        ]);
    }
}