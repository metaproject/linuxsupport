<?php
namespace App\Http\Controllers\Admin\Traits;
use App;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

trait ComputerManagerTrait
{
    public function addComputer(Request $request){
        $this->addComputerValidator($request->all())->validate();
        if(App\Computer::where('order_id',$request->order_id)->count()<App\Oder::with('order_type')->find($request->order_id)->order_type->computers) {
            $newComputer = new App\Computer;
            $newComputer->order_id = $request->order_id;
            $newComputer->code = $request->code;
            if ($newComputer->save()) {
                //return new list
                return response()->json(['success' => 'Computer added successfully']);
            }
        }else{
            return response()->json(['error' => 'This plan can`t contain more computers']);
        }
    }
    public function updateComputer(Request $request){
        $this->updateComputerValidator($request->all())->validate();
        $Computer=App\Computer::find($request->id);
        $Computer->code=$request->code;
        if ($Computer->save()) {
            //return new list
            return response()->json(['success' => 'Computer updated successfully']);
        }
    }
    protected function addComputerValidator(array $data)
    {
        return Validator::make($data, [
            'order_id'=>'required|int|exists:orders,id',
            'code'=>'required|int',
        ]);
    }
    protected function updateComputerValidator(array $data)
    {
        return Validator::make($data, [
            'id'=>'required|int|exists:computers,id',
            'code'=>'required|int',
        ]);
    }
}