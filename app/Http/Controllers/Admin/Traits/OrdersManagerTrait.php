<?php

namespace App\Http\Controllers\Admin\Traits;
use App;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Auth;

trait OrdersManagerTrait
{
    public function takeOrder(Request $request){
        $this->takeOrderValidator(['id'=>$request->id])->validate();
        $order = App\order::find($request->id);
        if(Auth::user()->role_id==2) {
            $order->manager_id = App\Manager::where('user_id',Auth::user()->id)->first()->id;
        }
        if ($order->save()) {
            //return new list
            return redirect()->route('adminOrders');
        }
    }
    protected function takeOrderValidator(array $data)
    {
        return Validator::make($data, [
            'id' => 'required|int'
        ]);
    }
}
