<?php

namespace App\Http\Controllers\Admin\Traits;
use App;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

trait ManagersAdminTrait
{
    public function addManager(Request $request)
    {
        $forvalidate = $request->all();
        $forvalidate['managers_count'] = App\User::where('role_id', 2)->count();
        if ($request->id) {
            //if user exist
            $this->addManagerValidator($forvalidate)->validate();
            $user = App\User::find($request->id);
            if ($user->role_id >= 3) {
                return response()->json(['errors' => 'User`s role can`t be changed']);
            }
        } else {
            //adding new user
            $this->addManagerAndUserValidator($forvalidate)->validate();
            $request->merge(['password' => Hash::make($request->password)]);
            //add photo
            if ($content = file_get_contents($request->photo)) {
                $arr_url = explode('/', $request->photo);
                $name = uniqid() . array_pop($arr_url);
                if (Storage::disk('public')->put($name, $content)) {
                    $photo = Storage::url($name);
                }
            }
            $user = User::create(['name'=>$request->name,'password'=>$request->password,'photo'=>$photo,
                'active'=>1]);
        }
        //set user`s role as manager
        $user->role_id = 2;
        if ($user->save()) {
            //return new list
            $managers = App\User::where('role_id', 2)->with('communication', 'communication.communication', 'managementCount')->orderBy('updated_at', 'desc')->paginate(10);
            return response()->json(['success' => 'Manager added successfully', $managers]);
        }
    }
    public function deleteManager(Request $request){
        $this->deleteManagerValidator($request->all())->validate();
        $user = App\User::find($request->id);
        if($user->role_id==2) {
            $user->role_id = 3;
            if ($user->save()) {
                $managers=App\User::where('role_id',2)->with('communication','communication.communication','managementCount')->orderBy('updated_at','desc')->paginate(10);
                return response()->json(['success'=>'Manager deleted successfully',$managers]);
            }
        }else{
            return response()->json(['errors'=>'User can`t be deleted']);
        }
    }
    protected function addManagerValidator(array $data)
    {
        return Validator::make($data, [
            'id' => 'required|int',
            'managers_count'=>'max:9',
        ]);
    }
    protected function addManagerAndUserValidator(array $data)
    {
        return Validator::make($data, [
            'name'=>'required|string|min:5|max:15',
            'password'=>'required|string|min:6|max:15',
            'photo' => 'required|url|mimes:jpg,png|string|min:5|max:255',
            'managers_count'=>'max:9'
        ]);
    }
    protected function deleteManagerValidator(array $data)
    {
        return Validator::make($data, [
            'id' => 'required|int'
        ]);
    }
}
