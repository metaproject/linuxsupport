<?php

namespace App\Http\Controllers\Admin\Traits;
use App;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;

trait OrdersAdminTrait
{
    public function changeOrder(Request $request)
    {
        $this->changeOrderValidator($request->all())->validate();
        $order = App\order::find($request->id);
        if ($request->client_id)
            $order->client_id = $request->client_id;
        if ($request->type_id)
            $order->type_id = $request->type_id;
        if ($request->status_id) {
            $order->status_id = $request->status_id;
            if ($request->status_id == 3)
                $order->done_at = Carbon::now();
        }
        if ($request->manager_id)
            $order->manager_id = $request->manager_id;
        if ($order->save()) {
            //return new list
            return response()->json(['success' => 'Article added successfully']);
        }
    }
    protected function changeOrderValidator(array $data)
    {
        return Validator::make($data, [
            'id' => 'required|int',
            'client_id'=>'int|exists:client,id',
            'type_id'=>'int|exists:order_type,id',
            'status_id'=>'int|exists:orders_statuses,id',
            'manager_id'=>'int|exists:managers,id'
        ]);
    }
}
