<?php

namespace App\Http\Controllers\Admin;
use App;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class PanelController extends Controllers\Controller
{
    use Traits\SearchTrait;
    //base controller for all pages in admin panel
    public function index()
    {
        return view('admin.home');
    }
    public function orders(Request $request)
    {
        $lang = collect([App\Language::where('name',App::getLocale())->first(),App\Language::where('name','!=',App::getLocale())->first()]);
        if($request->search){
            $orders = App\Order::whereIn('id', json_decode($request->search))->with('order_status', 'order_type.translation', 'client.communication', 'client.user.phone', 'client.user.communication.communication', 'handling', 'manager.user')->orderBy('created_at', 'desc')->paginate(10);
        }else {
            $orders = App\Order::where('status_id', '<', 3)->with('order_status', 'order_type.translation', 'client.communication', 'client.user.phone', 'client.user.communication.communication', 'handling', 'manager.user')->orderBy('created_at', 'desc')->paginate(10);
        }
        $statuses=App\Order_status::all();
        $managers=App\Manager::with('user')->get();
        $role=App\Role::whereHas('user',function ($q){
            $q->where('id',Auth::user()->id);
        })->first()->name;
        $title='Orders';
        $last_seen=App\Last_seen::where('user_id',Auth::user()->id)->where('subject_type_id',1)->first();
        $last_seen->time=Carbon::now();
        $last_seen->save();
        return view('admin.orders',compact('orders','statuses','managers','role','lang','title'));
    }
    public function clients()
    {
        $clients=App\Client::with('user','user.phone','communication')->paginate(10);
        $title='Clients';
        return view('admin.clients',compact('clients','title'));
    }
    public function managers()
    {
        $managers=App\Manager::with('user.communication.communication','managementCount')->orderBy('updated_at','desc')->paginate(10);
        $title='Managers';
        return view('admin.managers',compact('managers','title'));
    }
    public function archive()
    {
        $clients=App\Client::with('user.communication.communication')->orderBy('created_at','desc')->paginate(10);
        $orders=App\Order::where('status_id',3)->with('client.user','handling','order_rate')->orderBy('created_at','desc')->paginate(10);
        $title='Archive';

        return view('admin.archive',compact('clients','orders','title'));
    }
    public function blog()
    {
        $lang = App\Language::where('name',App::getLocale())->first();
        $articles=App\Blog::with('user','tags','translation')->whereHas('translation',function($q) use ($lang)
        {
            $q->where('language_id', $lang->id);

        })->orderBy('created_at','desc')->paginate(10);
        $title='Blog';
        return view('admin.blog',compact('articles','title'));
    }
    public function notes()
    {
        $lang = App\Language::where('name',App::getLocale())->first();
        $notes=App\Note::with('translation')->whereHas('translation',function($q) use ($lang)
        {
            $q->where('language_id', $lang->id);

        })->orderBy('created_at','desc')->paginate(10);
        $note_types=App\Note_Type::all();
        $title='Notes';
        return view('admin.notes',compact('notes','note_types','title'));
    }
    public function fields()
    {
        $fields=App\Custom_Field::paginate(10);
        $title='Custom Fields';
        return view('admin.fields',compact('fields','title'));
    }
}
