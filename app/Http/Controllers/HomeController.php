<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use Auth;


class HomeController extends Controller
{
    public function index(Request $request)
    {
        //redirection if have order
        if(session('redirected')===0&&Auth::check()&&Auth::user()->active==1){
            return redirect()->route('payForOrder');
        }
        //language
        $lang = collect([App\Language::where('name',App::getLocale())->first(),App\Language::where('name','!=',App::getLocale())->first()]);
        $communications=App\Communication::all();
        $support_types=App\Order_type::all();
        //notes
        $notes_unsorted=App\Note::with('translation','type')->whereHas('translation',function($q) use ($lang)
        {
            $q->where('language_id', $lang->first())->orWhere('language_id', '=', 1);

        })->get();
        $notes=array();
        foreach($notes_unsorted as $note)
        {
            $key=$note->type->name;
            $notes[$key][]=$note;
        }

        //blog
        $blog=App\Blog::with('translation')->whereHas('translation',function($q) use ($lang)
        {
            $q->where('language_id', $lang->first())->orWhere('language_id', '=', 1);

        })->orderBy('created_at', 'desc')->take(2)->get();
        return view('home',compact('lang','blog','support_types','communications','notes'));
    }
}
