<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use App\GlobalTraits;
use Carbon\Carbon;
use Auth;

class OrderController extends Controller
{
    use GlobalTraits\SetSession;

    public function PayForOrder(){
        //prevent more redirection than one on this page
        session(['redirected' => 1]);
        $session = collect(session('order'));
        $order_type=App\Order_type::with('translation')->find($session['order']['type']);
        $lang = collect([App\Language::where('name',App::getLocale())->first(),App\Language::where('name','!=',App::getLocale())->first()]);
        return view('order.payment',['lang'=>$lang,'session'=>$session,'order_type'=>$order_type]);
    }
    public function changePlan(Request $request)
    {
        $order=App\Order::find($request->order_id);
        $old_plan=App\Order_type::where('id',$order->type_id)->first();
        $new_plan=App\Order_type::where('id',$request->new_plan)->first();
        if($old_plan->cost<$new_plan->cost){
            if($old_plan->period!='one-time') {
                $newDaysForExpire = $order->payment_date->addDays($this->period_length($new_plan,$order))->diffinDays(Carbon::now());

                $cost = $order->days_to_expire * ($new_plan->cost / $this->period_length($new_plan,$order) - $old_plan->cost / $this->period_length($old_plan,$order))+(($newDaysForExpire-$order->days_to_expire)*$new_plan->cost);
            }else{
                $cost =  $new_plan->cost-$old_plan->cost;
            }
            print_r([$order->expire_date,$this->period_length($old_plan,$order),$this->period_length($new_plan,$order),$cost]);
            $prefer=App\Client::where('user_id',Auth::user()->id)->first()->communication_channel_id;
            $phone=App\Communication_coordinate::where('user_id',Auth::user()->id)->where('communication_channel_id',6)->first();
            $this->changePlanSession($prefer,$phone,$cost,$new_plan->id);
            return redirect()->route('payForOrder');
        }
        $order->type_id=$request->new_plan;
        $order->save();
        \Session::flash('flash_message', trans('interface.changeOrderType'));
        return redirect()->route('personalHome');
    }
    public function renewalOrder(Request $request){
        $this->renewalOrderValidator($request->all())->validate();
        $userId = Auth::id();
        $client_id=App\Client::where('user_id',$userId)->first()->id;
        $order=App\Order::where('client_id',$client_id)->find($request->order_id);
        if($order) {
            $order->payment_date = Carbon::now();
            $order->save();
            return response()->json(['success' => 'Plan has updated']);
        }
    }
    public function addHandling(Request $request)
    {
        $this->addHandlingValidator($request->all())->validate();
        $handling = new App\Handling;
        //add Handling
        $handling->order_id = $request->order_id;
        $new_id=($last_id=App\Handling::where('order_id',$request->order_id)->orderBy('order_id', 'desc')->first()->order_id)?$last_id+1:0;
        $handling->handling =$new_id;
        $handling->problem = $request->problem;
        if ($handling->save()) {
            return response()->json(['success' => 'Handling added successfully']);
        }

    }
    protected function renewalOrderValidator(array $data)
    {
        return Validator::make($data, [
            'order_id'=>'required|int|exists:orders,id'
        ]);
    }
    protected function addHandlingValidator(array $data)
    {
        return Validator::make($data, [
            'problem'=>'required|string|max:500',
            'order_id'=>'required|int|exists:order,id',
        ]);
    }
    public function period_length($type,$order)
    {
        if($type->period=='one-time') {
            return 1;
        }else{
            $payment_date=$order->payment_date;
            return $payment_date->addMonth(1)->diffinDays($order->payment_date);
        }
    }
}
