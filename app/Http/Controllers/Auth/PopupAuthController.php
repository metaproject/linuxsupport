<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class PopupAuthController extends Controller
{
    public function Login(Request $request)
    {
        if ($user=App\User::where('email', $request->email)->first()) {
            if($user->active!=1){
                return response()->json(['error'=>trans('messages.ActivatePlease3')]);
            }
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                // success
                return response()->json(['success'=>'Wellcome!']);
            }

        }
        return response()->json(['error'=>'Wrong email or password!']);
    }

}
