<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LogoutController extends Controller
{

    public function logout(){
        Auth::logout();
        return response()->json(['success'=>'Logout!']);
    }
}
