<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Auth;
use Mail;
use App\Mail as Mails;

class ActivateController extends Controller
{

    public function activation($userId, $token)
    {
        $user = User::findOrFail($userId);

        // Check token in user DB. if null then check data (user make first activation).
        if (is_null($user->remember_token)) {
            // Check token from url.
            if (md5($user->email) == $token) {
                // Change status and login user.
                $user->active = 1;
                $user->save();

                \Session::flash('flash_message', trans('interface.ActivatedSuccess'));

                // Make login user.
                Auth::login($user, true);
            } else {
                // Wrong token.
                \Session::flash('flash_message_error', trans('interface.ActivatedWrong'));
            }
        } else {
            // User was activated early.
            \Session::flash('flash_message_error', trans('interface.ActivatedAlready'));
        }
        return redirect()->route('home');
    }
    public function newMessage(){
        Mail::to(Auth::user())->send(new Mails\ActivateAccount(Auth::user()));
        return redirect()->route('home');
    }
}
