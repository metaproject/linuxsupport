<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Mail;
use Illuminate\Support\Facades\Hash;
use App;

class SocialAuthController extends Controller
{

    public function auth($provider)
    {
        $user = \Socialite::driver($provider)->user();

        // Find user in DB.
        $userData = User::where('email', $user->email)->first();

        // Check exist user.
        if (isset($userData->id)) {

            // Check user status.
            if ($userData->active) {

                // Make login user.
                Auth::loginUsingId($userData->id, TRUE);
            }
            // Wrong status.
            else {
                \Session::flash('flash_message_error', trans('messages.ActivatePlease1'));
            }

            return redirect()->route('home');
        }
        // Make registration new user.
        else {

            // Create new user in DB.
            $new_user = new App\User;
            $new_user->name=$user->name;
            $new_user->photo=$user->avatar;
            $new_user->email=$user->email;
            $new_user->password=Hash::make(str_random(8));
            $new_user->active = 1;
            $new_user->save();
            // Make login user.
            Auth::loginUsingId($new_user->id, TRUE);

            \Session::flash('flash_message', trans('messages.ActivatedSuccess'));

            return redirect()->route('home');
        }
    }
}
