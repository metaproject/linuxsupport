<?php
namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App;
use Auth;


class AdminMenuComposer {
    public function compose(View $view)
    {
        if(Auth::check())
        {
            $user=App\User::with('phone')->where('id',Auth::user()->id)->first();
            $view->with('user', $user);
            $last_seen=App\Last_seen::where('user_id',Auth::user()->id)->where('subject_type_id',1)->first();
            if(is_object($last_seen)){
                $new_orders_count=App\Order::where('updated_at','>',$last_seen->time)->count();
                $view->with('new_orders_count', $new_orders_count);
            }


        }
        $view->with('menu', App\Navigation::where('menu_type_id',1)->get());

    }

}