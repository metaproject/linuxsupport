<?php
namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App;
use App\GlobalTraits;
use Auth;

class ProfileComposer {

    use GlobalTraits\PhoneMutator;
    public function compose(View $view)
    {
        //custom fields
        $fields=App\Custom_Field::all();
        $custom_fields=array();
        foreach($fields as $field)
        {
            $key=$field->alias;
            $custom_fields[$key]=$field;
        }
        $support_types=App\Order_type::all();
        $custom_fields['our_phone']->value='+'.$this->phone($custom_fields['our_phone']->value,false);
        $user=array();
        if(Auth::check())
        {
            $user=App\User::with('phone')->where('id',Auth::user()->id)->first();
        }
        $view->with('custom_fields', $custom_fields);
        $view->with('support_types', $support_types);
        $view->with('user', $user);
    }

}