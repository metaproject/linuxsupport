<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_rate extends Model
{
    protected $table = 'orders_rates';
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
