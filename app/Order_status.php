<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_status extends Model
{
    protected $table = 'orders_statuses';
    public $timestamps = false;
    public function order()
    {
        return $this->hasMany('App\Order');
    }
}
