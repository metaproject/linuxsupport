<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Last_seen_type extends Model
{
    protected $table = 'last_seen_types';
    public $timestamps = false;
    public function last_seen()
    {
        return $this->hasMany('App\Last_seen');
    }
    //
}
