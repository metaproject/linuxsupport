<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order extends Model
{
    protected $dates = ['payment_date','expire_date','created_at'];
    public function client()
    {
        return $this->belongsTo('App\Client');
    }
    public function order_type()
    {
        return $this->belongsTo('App\Order_type','type_id');
    }
    public function order_status()
    {
        return $this->belongsTo('App\Order_status','status_id');
    }
    public function order_rate()
    {
        return $this->hasMany('App\Order_rate');
    }
    public function handling()
    {
        return $this->hasMany('App\Handling');
    }
    public function manager()
    {
        return $this->belongsTo('App\Manager');
    }
    public function computers()
    {
        return $this->hasMany('App\Computer');
    }
    public function getExpireDateAttribute()
    {
        return $this->payment_date->addDays($this->order_type()->first()->period_length);
    }
    public function getDaysToExpireAttribute()
    {
        return $this->getExpireDateAttribute()->diffinDays(Carbon::now());
    }
    public function getActiveAttribute()
    {
        return $this->getExpireDateAttribute()<Carbon::now();
    }

}
