<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note_Type extends Model
{
    protected $table = 'note_types';
    public $timestamps = false;
    public function note(){
        return $this->hasMany('App\Note');
    }
}
