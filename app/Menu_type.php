<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu_type extends Model
{
    public $timestamps = false;
    protected $table = 'menu_types';
    public function navigation()
    {
        return $this->hasMany('App\Navigation');
    }
}
