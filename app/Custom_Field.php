<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Custom_Field extends Model
{
    public $timestamps = false;
    protected $table = 'custom_fields';
}
