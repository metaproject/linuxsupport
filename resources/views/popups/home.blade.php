
<div class="popup-bg">
    <div class="popup">
        <div class="popup-close"></div>
        <div class="popup-container">
            <div class="popup-title">{{ trans('forms/home_popup.title') }}</div>
            <div class="popup-labels">
                <div class="head-main-right-text">{{ trans('forms/home_popup.name_title') }}</div>
                <div class="head-main-right-text">{{ trans('forms/home_popup.phone_title') }}</div>
            </div>
            <form action="" method="POST">
            <div class="popup-inputs">
                <input required placeholder='{{ trans('forms/home_popup.name_placeholder') }}' value="{{ ($user)?$user->name:'' }}" type="text" id="input1" class="head-main-right-input inputp">
                <input required placeholder='{{ trans('forms/home_popup.phone_placeholder') }}' value="{{ ($user&&count($user->phone)>0)?$user->phone[0]->coordinate:'' }}"  type="text" id="input2" class="head-main-right-input inputp">
            </div>
            <div class="head-main-right-icons popup-icons">
                <div tabindex="0" class="head-main-right-icons-icon tl" data-id='1' title="Telegram"></div>
                <div tabindex="0" class="head-main-right-icons-icon wh" data-id='2' title="Whatsapp"></div>
                <div tabindex="0" class="head-main-right-icons-icon vb" data-id='3' title="Viber"></div>
                <div tabindex="0" class="head-main-right-icons-icon sk" data-id='4' title="Skype"></div>
                <div tabindex="0" class="head-main-right-icons-icon em" data-id='5' title="Email"></div>
                <div tabindex="0" class="head-main-right-icons-icon cl" data-id='6' title="Call"></div>
            </div>
            <div class="popup-plans-title">{{ trans('forms/home_popup.plan_title') }}</div>
            <div class="popup-plans">
                <div tabindex="0" class="popup-plans-plan" data-id='1'><div class="popup-plans-plan-text">{{ trans('forms/home_popup.type1') }}<br />${{ $support_types[0]->cost }}</div></div>
                <div tabindex="0" class="popup-plans-plan" data-id='2'><div class="popup-plans-plan-text">{{ trans('forms/home_popup.type2') }}<br />${{ $support_types[1]->cost }}</div></div>
                <div tabindex="0" class="popup-plans-plan" data-id='3'><div class="popup-plans-plan-text">{{ trans('forms/home_popup.type3') }} ${{ $support_types[2]->cost }}</div></div>
            </div>
            <div class="popup-input-title">{{ trans('forms/home_popup.descr_title') }}</div>
            <textarea placeholder="{{ trans('forms/home_popup.descr_placeholder') }}" name="description" class="popup-input"></textarea>
                <input style="display: block; border: none" type="submit" tabindex="0" class="popup-button" value="{{ trans('forms/home_popup.button') }}">
            </form>
        </div>
    </div>
</div>
<div class="signin-bg" id="signInBg">
    <div class="signin">
        <div class="signin-close" id="signInClose"></div>
        <div class="signin-container">
            <div class="signin-title">{{ trans('forms/popup_register.title') }}</div>
            <form action="" method="POST">
                <div class="signin-label">{{ trans('forms/popup_register.name_title') }}</div>
                <input required type="text" id="register_name" class="signin-input" placeholder="{{ trans('forms/popup_register.name_placeholder') }}">
                <div class="signin-label">{{ trans('forms/popup_register.email_title') }}</div>
                <input required type="text" id="register_email" class="signin-input at" placeholder="{{ trans('forms/popup_register.email_placeholder') }}">
                <div class="signin-label">{{ trans('forms/popup_register.password_title') }}</div>
                <input required type="password" id="register_password1" class="signin-input locked" placeholder="{{ trans('forms/popup_register.password_placeholder') }}">
                <div class="signin-label">Repeat password:</div>
                <input required type="password" id="register_password2" class="signin-input locked" placeholder="Repeat your password">
                <input style="display: block; border: none" type="submit" tabindex="0" class="signin-button" value="{{ trans('forms/popup_register.button') }}">
            </form>
            <div class="signin-or">&nbsp;{{ trans('forms/popup_register.soc_title') }}&nbsp;</div>
            <div class="signin-buttons">
                <div class="signin-buttons-button facebook">Facebook</div>
                <div class="signin-buttons-button google">Google</div>
            </div>
            <div class="signin-last">{{ trans('forms/popup_register.bottom1') }}<a href="#" class="logIn">{{ trans('forms/popup_register.bottom2') }}</a></div>
        </div>
    </div>
</div>
<div class="signin-bg" id="logInBg">
    <div class="signin">
        <div class="signin-close" id="logInClose"></div>
        <div class="signin-container">
            <div class="signin-title">{{ trans('forms/popup_login.title') }}</div>
            <form action="" method="POST">
                <div class="signin-label">{{ trans('forms/popup_login.email_title') }}</div>
                <input required type="text" id="login_email" class="signin-input at" placeholder="{{ trans('forms/popup_login.email_placeholder') }}">
                <div class="signin-label">{{ trans('forms/popup_login.password_title') }}</div>
                <input required type="password" id="login_password" class="signin-input locked" placeholder="{{ trans('forms/popup_login.password_placeholder') }}">
                <a href="{{ route('password.request') }}"><div class="signin-forgot">{{ trans('forms/popup_login.forgot_title') }}</div></a>
                <input type="submit" style="display: block; border: none" class="signin-button" value="{{ trans('forms/popup_login.button') }}">
            </form>
            <div class="signin-or">&nbsp;{{ trans('forms/popup_login.soc_title') }}&nbsp;</div>
            <div class="signin-buttons">
                <div class="signin-buttons-button facebook">Facebook</div>
                <div class="signin-buttons-button google">Google</div>
            </div>
            <div class="signin-last">{{ trans('forms/popup_login.bottom1') }}<a href="#" id="signIn">{{ trans('forms/popup_login.bottom2') }}</a></div>
        </div>
    </div>
</div>