<?php
$total=$paginate->total();
$perPage = $paginate->perPage();
$lastPage = ceil($total/$perPage);
$curPage = $paginate->currentPage();
?>
@if($lastPage>1)
<div class="slider-nav">
    <?php $url = $paginate->url(1) ?>
    <a href="{{ $url }}"><div class="slider-nav-prev">&lt;</div></a>
        <div class="slider-nav-dots">
        <?php $p=1 ?>
        @while($p<=$lastPage)
            <?php $url = $paginate->url($p) ?>
        @if($p>$curPage+3)
                    <?php $p=$lastPage ?>
                    <div class="slider-nav-dots-dots">...</div>
                    <div class="slider-nav-dots-dot">{{ $lastPage }}</div>
            @else
                    <a href="{{ $url }}"><div class="slider-nav-dots-dot {{ ($curPage == $p)?'admin-slider-active':'' }}">{{ $p }}</div></a>
                @endif
            @if($p<$curPage-3)
                    <div class="slider-nav-dots-dots">...</div>
                    <?php $p=$curPage-3; ?>
                @else
                <?php $p++; ?>
                @endif
        @endwhile
        <?php $url = $paginate->url($lastPage) ?>
        </div>
        <a href="{{ $url }}"><div class="slider-nav-next">&gt;</div></a>

</div>
    @endif