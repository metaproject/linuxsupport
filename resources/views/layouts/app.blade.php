<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
@yield('meta')
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Linux @yield('title')</title>

    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <script>//errors
        
        var first_form_phone='{{ trans('messages.first_form_phone') }}',
            first_form_com='{{ trans('messages.first_form_com') }}',
            register_no_email='{{ trans('messages.register_no_email') }}',
            register_no_password='{{ trans('messages.register_no_password') }}',
            register_not_matched='{{ trans('messages.register_not_matched') }}',
            register_no_name='{{ trans('messages.register_no_name') }}';
    </script>
</head>
<body class="home" style="opacity:0;transition: opacity .2s;">
<div class="@yield('page')">
@include('popups.home')
<div class="header">
    <div class="header-container">
        <a tabindex="0" class="header-logo" onclick="return clickHandler()" href="/"><span class="header-logo-bold">LINUX</span>Support</a>
        <div class="header-right">
            <a tabindex="0" style="color: #fff; text-decoration: none" href="tel:{{ $custom_fields['our_phone']->value }}" class="header-right-phone">{{ $custom_fields['our_phone']->value }}</a>

            <div class="header-right-lang-wrapper">
                <div tabindex="0" class="header-right-lang-wrapper-lang" id="lang" style="outline: none">{{ strtoupper($lang->first()->name) }}</div>
                <div class="ru"><a href="/setlocale/{{ $lang->get(1)->name }}">{{ strtoupper($lang->get(1)->name) }}</a></div>
            </div>
            <a tabindex="0" style="color: #fff; text-decoration: none" href="tel:{{ $custom_fields['our_phone']->value }}" class="header-right-phone-mobile"></a>

            @guest
            <button tabindex="0" class="header-right-button logIn">{{ trans('homepage.buttons')['header_login'] }}</button>

            @else
                <div tabindex="0" class="header-right-user"><a style="text-decoration: none; color: #fff" href="{{ route('personalHome') }}">{{ $user->name }}</a><a class="header-right-logout"></a></div>
            
                @endguest
                <a class="header-right-button-mobile logIn"></a>
        </div>
    </div>
</div>

@yield('content')
 <a href="javascript:" id="return-to-top">&uarr;</a>
<div class="footer">
    <div class="footer-line"></div>
    <div class="footer-container">
        <div class="footer-top">
            <div class="footer-top-title">{{ trans('homepage.footer_subtitle') }}<div class="footer-top-title-bold">{{ trans('homepage.footer_title') }}</div></div>
            <div tabindex="0" class="footer-top-button popupShow" id="popupShow">{{ trans('homepage.buttons')['bottom_button1'] }}</div>
        </div>
        <div class="footer-bottom">
            <div class="footer-bottom-phone">{{ $custom_fields['our_phone']->value }}</div>
            <div class="footer-bottom-copyright">© LinuxSupport. {{ trans('homepage.footer_rights') }}</div>
        </div>
    </div>
</div>
</div>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'MmkMjizwi8';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>
</html>
<script src="{{ asset('js/detect.min.js') }}" defer></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/script.js') }}" defer></script>
<script>
    $(document).ready(function() {
        $("body").css("opacity", "1");

    });
</script>
