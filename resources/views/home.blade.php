@extends('layouts.app')
@section('title', 'Support')
@section('page', 'index')
@section('content')
    <div class="head">
        <div class="container">
            <div class="head-main">
                <div class="head-main-left">
                    <div class="head-main-left-title">{{ trans('homepage.first_title') }}</div>
                    <div class="head-main-left-text">{{ trans('homepage.first_text') }}</div>
                    <div id="wrapper">
                        <div id="wrapper-inner">
                            <div id="scroll-down">
                            <span class="arrow-down">
                            </span>
                                <span id="scroll-title" style="{{ ($lang->first()->name=='ru')?'font-size:13.9px':'' }}">{{ trans('homepage.buttons')['first_button1'] }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="head-main-right">
                    <div class="head-main-right-container">
                    <div class="head-main-right-title">{{ trans('forms/home_request.title') }}</div>
                        <form action="">
                                <div class="head-main-right-text">{{ trans('forms/home_request.phone_title') }}</div>
                                <input style="width: calc(100% - 37px)" required placeholder='{{ trans('forms/home_request.phone_placeholder') }} 'type="text" value="{{ (isset($user)&&count($user->phone)>0)?$user->phone[0]->coordinate:'' }}"  name="phone" id="inputRight" class="head-main-right-input inputr">
                                <div class="head-main-right-icons">
                                    <div tabindex="0" class="head-main-right-icons-icon tl" data-id='1' title="Telegram"></div>
                                    <div tabindex="0" class="head-main-right-icons-icon wh" data-id='2' title="Whatsapp"></div>
                                    <div tabindex="0" class="head-main-right-icons-icon vb" data-id='3' title="Viber"></div>
                                    <div tabindex="0" class="head-main-right-icons-icon sk" data-id='4' title="Skype"></div>
                                    <div tabindex="0" class="head-main-right-icons-icon em" data-id='5' title="Email"></div>
                                    <div tabindex="0" class="head-main-right-icons-icon cl" data-id='6' title="Call"></div>
                                </div>
                                <div tabindex="0" class="head-main-right-info">
                                    <div class="head-main-right-info-item chosen-plan" data-id="1"><span class="chosen-plan-border">{{ trans('forms/home_request.type1',['cost' => $support_types[0]->cost]) }}</span></div>
                                    <div class="head-main-right-info-item" data-id="2"><span>{{ trans('forms/home_request.type2',['cost' => $support_types[1]->cost]) }}</span></div>
                                    <div class="head-main-right-info-item" data-id="3"><span>{{ trans('forms/home_request.type3',['cost' => $support_types[2]->cost]) }}</span></div></div>

                            <input type="submit" style="display: block; border: none; width: calc(100% - 12px)" class="head-main-right-button" value="{{ trans('forms/home_request.button') }}">
                        </form>
                    </div>
                </div>
                <div id="wrapperm">
                    <div id="wrapper-innerm">
                        <div id="scroll-downm">
                        <span class="arrow-down">
                        </span>
                            <span id="scroll-titlem" style="{{ ($lang[0]->name=='ru')?'font-size:13.9px':'' }}">{{ trans('homepage.buttons')['first_button1'] }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(session('flash_message'))
        <div class="alert alert-success">
            {{ session('flash_message') }}
        </div>
        @endif
    @if(session('flash_message_error'))
        <div class="alert alert-danger">
            {!! session('flash_message_error') !!}
        </div>
    @endif
    <div class="main">
        <a class="anchor1" id="anchor1"></a>
        <div class="main-pretitle">{{ trans('homepage.second_title1') }}</div>
        <div class="main-title">{{ trans('homepage.second_subtitle1') }}</div>
        <div class="main-path">
            <div class="main-path-item">
                <div class="main-path-item-arrow"></div>
                <div class="mobile-arrow"></div>
                <div class="main-path-item-number">1</div>
                <div class="main-path-item-title">{{ $notes['How it Works?'][0]->translation[0]->title }}</div>
                <div class="main-path-item-text">{{ $notes['How it Works?'][0]->translation[0]->text }}</div>
            </div>
            <div class="main-path-item">
                <div class="main-path-item-arrow arrow2"></div>
                <div class="mobile-arrow2"></div>
                <div class="main-path-item-number">2</div>
                <div class="main-path-item-title second">{{ $notes['How it Works?'][1]->translation[0]->title }}</div>
                <div class="main-path-item-text secondtext">{{ $notes['How it Works?'][1]->translation[0]->text }}</div>
            </div>
            <div class="main-path-item">
                <div class="main-path-item-arrow arrow3"></div>
                <div class="mobile-arrow"></div>
                <div class="main-path-item-number">3</div>
                <div class="main-path-item-title">{{ $notes['How it Works?'][2]->translation[0]->title }}</div>
                <div class="main-path-item-text">{{ $notes['How it Works?'][2]->translation[0]->text }}</div>
            </div>
        </div>
        <div class="main-instruction">
            <div class="main-instruction-left">
                <div class="main-instruction-left-title">{{ $notes['How it Works?'][3]->translation[0]->title }}</div>
                <div class="main-instruction-left-description">{{ $notes['How it Works?'][3]->translation[0]->text }}</div>
                <div class="main-instruction-left-text">All you need is to install Teamviewer and provide us special code. You will see every actions of our personal, so everything is safe</div>
                <a href="/inform"><div class="main-instruction-left-button">{{ trans('homepage.buttons')['second_button1'] }}</div></a>
                <div class="main-instruction-left-buttondesc">*{{ trans('homepage.second_note_footnote') }}</div>
            </div>
            <div class="main-instruction-or">or</div>
            <div class="main-instruction-right">
                <div class="main-instruction-right-title">Step-to-Step Instruction</div>
                <div class="main-instruction-right-description">{{ $notes['How it Works?'][4]->translation[0]->text }}</div>
                <div class="main-instruction-right-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris </div>
            </div>
        </div>
        <div tabindex="0" class="main-button popupShow">{{ trans('homepage.buttons')['second_button2'] }}</div>
    </div>
    <div class="info">
        <div class="info-pretitle">{{ trans('homepage.second_subtitle2') }}</div>
        <div class="info-title">{{ trans('homepage.second_title2') }}</div>
        <div class="info-reasons">
            @foreach($notes['A few reasons'] as $note)
                <div class="info-reasons-reason">
                    <div class="info-reasons-reason-icon" style="background-image: url({{ asset($note->image) }})"></div>
                    <div class="info-reasons-reason-title">{{  $note->translation[0]->title }}</div>
                    <div class="info-reasons-reason-text">{{  $note->translation[0]->text }}</div>
                </div>
                @endforeach
        </div>
    </div>
    <div class="services">
        <div class="services-pretitle">{{ trans('homepage.third_subtitle2') }}</div>
        <div class="services-title">{{ trans('homepage.third_title2') }}</div>
        <div class="services-items">
            <div class="services-items-itemone">
                <div class="services-items-itemone-title">{{ trans('forms/home_onetime.title') }}</div>
                <div class="services-items-itemone-text">{{ trans('forms/home_onetime.text') }}</div>
                <div tabindex="0" class="services-items-itemone-button popupShow1">{{ trans('homepage.buttons')['third_button1'] }}</div>
                <div class="services-items-itemone-price"><span class="services-items-itemone-pricedollar">$</span> {{  $support_types[0]->cost }}</div>
            </div>
            <div class="services-items-item">
                <div class="services-items-item-title">{{ trans('forms/home_month.title') }}</div>
                <ul class="services-items-item-list">
                    <li class="services-items-item-list-item">{{ trans('forms/home_month.li1') }}</li>
                    <li class="services-items-item-list-item">{{ trans('forms/home_month.li2') }}</li>
                    <li class="services-items-item-list-item">{{ trans('forms/home_month.li3',['number'=>1]) }}</li>
                    <li class="services-items-item-list-item">{{ trans('forms/home_month.li4',['cost'=>$support_types[1]->cost]) }}</li>
                </ul>
                <div tabindex="0" class="services-items-item-button popupShow2">{{ trans('homepage.buttons')['third_button1'] }}</div>
            </div>
            <div class="services-items-item">
                <div class="services-items-item-title">{{ trans('forms/home_corporate.title') }}</div>
                <ul class="services-items-item-list">
                    <li class="services-items-item-list-item">{{ trans('forms/home_corporate.li1') }}</li>
                    <li class="services-items-item-list-item">{{ trans('forms/home_corporate.li2') }}</li>
                    <li class="services-items-item-list-item">{{ trans('forms/home_corporate.li3',['number'=>10]) }}</li>
                    <li class="services-items-item-list-item">{{ trans('forms/home_month.li4',['cost'=>$support_types[2]->cost]) }}</li>
                </ul>
                <div tabindex="0" class="services-items-item-button popupShow3">{{ trans('homepage.buttons')['third_button2'] }}</div>
            </div>
        </div>
    </div>
    <div class="testimonials">
        <div class="testimonials-pretitle">{{ trans('homepage.fourth_subtitle') }}</div>
        <div class="testimonials-title">{{ trans('homepage.fourth_title') }}</div>
        <div class="owl-carousel owl-theme slider">
            @foreach($notes['Reviews'] as $note)
                <div class="item">
                    <div class="slider-item">
                        <div class="slider-item-pic" style="background-image: url({{$note->image}})"></div>
                        <div class="slider-item-title">{{ $note->translation[0]->title }}</div>
                        <div class="slider-item-text">{{ $note->translation[0]->text }}</div>
                        <div class="slider-item-bg">“</div>
                    </div>
                </div>
            @endforeach

            </div>
        </div>
    <div class="blog">
        <div class="blog-pretitle">{{ trans('homepage.fifth_subtitle') }}</div>
        <div class="blog-title">{{ trans('homepage.fifth_title') }}</div>
        <div class="blog-items">
            @foreach($blog as $b)
                <div class="blog-items-item">
                    <img src="{{ asset($b->image) }}" alt="">
                    <div class="blog-items-item-title">{{ $b->translation[0]->title }}</div>
                    <div class="blog-items-item-text">{{ str_limit($b->translation[0]->text, $limit = 300, $end = '...') }}</div>
                </div>
                @endforeach
        </div>
        <div tabindex="0" class="blog-more" id="buttonBlog"><a href="{{ route('blogHome') }}"> {{ trans('homepage.buttons')['fifth_button1'] }}</a></div>
    </div>
    <div class="footerr">
        <div class="bgfoot"></div>
        <div class="faq">
            <div class="faq-pretitle">{{ trans('homepage.sixth_subtitle') }}</div>
            <div class="faq-title">{{ trans('homepage.sixth_title') }}</div>
            <div class="faq-main">
                <ul class="faq-main-list">
                    @foreach($notes['Questions'] as $question)
                        <li class="faq-main-list-item">{{ $question->translation[0]->title }}
                            <div class="faq-main-list-item-inside">{{ $question->translation[0]->text }}</div>
                        </li>
                        @endforeach

                </ul>
            </div>
        </div>
    </div>
@endsection
