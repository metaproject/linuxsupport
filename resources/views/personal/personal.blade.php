@extends('layouts.app')
@section('title', 'Personal Account')
@section('page', 'other')
@section('content')
    <div class="teamviewer personal">
        <div class="teamviewer-container per-acc-cont">
            <div class="per-acc-item">
                <a class="main-nav tv-nav"><a class="tv-link" onclick="return clickHandler()" href="/">Home |</a><a style="text-decoration: none" href="/personal" class="main-nav-green"> Personal account</a></a>
                <div class="teamviewer-title">Hello,<br>{{ Auth::user()->name }}!</div>
            </div>
            <div class="per-acc-sidebar">
                @if(is_object($orders->first()))
                <div class="per-acc-sidebar-item">
                    <div class="per-acc-sidebar-container">
                        <div class="per-acc-sidebar-pretitle">Your plan</div>
                        <div class="per-acc-sidebar-title changedTitle">{{ $orders->first()->order_type->translation[$lang->first()->id-1]->name }}</div>
                        <div class="per-acc-sidebar-date">{{ $orders->first()->payment_date->format('d.m.Y') }} - {{ $orders->first()->expire_date->format('d.m.Y') }}</div>
                        <div class="per-acc-sidebar-buttons">
                            <div class="per-acc-sidebar-buttons-button planChange">Change Plan</div>
                            <div class="per-acc-sidebar-buttons-button planUn">Unsubscribe</div>
                        </div>
                        <div class="per-acc-sidebar-planchange">
                            @foreach($orders_types->where('id','!=',$orders->first()->order_type->id)->all() as $order_type)
                                <div class="per-acc-sidebar-planchange-item"><a href="/personal/changePlan/{{$orders->first()->id}}/{{$order_type->id}}">{{ $order_type->translation[$lang->first()->id-1]->name }}</a></div>
                                @endforeach
                        </div>
                        <div class="per-acc-sidebar-sure">
                            <div class="per-acc-sidebar-sure-title">Are you sure?</div>
                            <div class="per-acc-sidebar-sure-button">Yes</div>
                        </div>
                    </div>
                </div>
                    @endif
            </div>
        </div>
    </div>
    @if(session('flash_message'))
        <div class="alert alert-success">
            {{ session('flash_message') }}
        </div>
    @endif
    @if(session('flash_message_error'))
        <div class="alert alert-danger">
            {!! session('flash_message_error') !!}
        </div>
    @endif
    <div class="per-acc-main">
        <div class="per-acc-main-container">
            <div class="per-acc-main-head">
                <div class="per-acc-main-head-item">History</div>
                <div class="per-acc-main-head-item per-acc-item-active2">Personal information</div>
            </div>
            <div class="per-acc-main-information block" id="paInformation">
                <div class="per-acc-main-information-label">Name:</div>
                <input type="text" class="per-acc-main-information-input pa-name" placeholder="Your name" value="{{ $user->name }}">
                <div class="per-acc-main-information-label">Contact Number:</div>
                <input type="text" class="per-acc-main-information-input pa-phone" placeholder="Your phone" value="{{ (is_object($phone=$user->communication->where('communication_channel_id',6)->first()))?$phone->coordinate:'' }}">
                <div class="head-main-right-icons">
                    <div tabindex="0" class="head-main-right-icons-icon tl {{ ($user->communication->where('communication_channel_id',1)->first()!==null)?'tlc':'' }}" title="Telegram"></div>
                    <div tabindex="0" class="head-main-right-icons-icon wh {{ ($user->communication->where('communication_channel_id',2)->first()!==null)?'whc':'' }}" title="Whatsapp"></div>
                    <div tabindex="0" class="head-main-right-icons-icon vb {{ ($user->communication->where('communication_channel_id',3)->first()!==null)?'vbc':'' }}" title="Viber"></div>
                    <div tabindex="0" class="head-main-right-icons-icon sk {{ ($user->communication->where('communication_channel_id',4)->first()!==null)?'skc':'' }}" title="Skype"></div>
                    <div tabindex="0" class="head-main-right-icons-icon em {{ ($user->communication->where('communication_channel_id',5)->first()!==null)?'emc':'' }}" title="Email"></div>
                    <div tabindex="0" class="head-main-right-icons-icon cl {{ ($user->communication->where('communication_channel_id',6)->first()!==null)?'clc':'' }}" title="Call"></div>
                </div>
                <div class="per-acc-main-information-label">Email:</div>
                <div class="change-wrapper">
                    <input type="text" class="per-acc-main-information-input pa-email" placeholder="Your email">
                    <div class="per-acc-main-information-buttonchange" id="changeEmail"></div>
                </div>
                <div class="per-acc-main-information-changeemail">
                    <div class="per-acc-main-information-label">New email:</div>
                    <input type="text" class="per-acc-main-information-input pa-email first" placeholder="Enter your new email">
                    <div class="per-acc-main-information-label">Repeat new email:</div>
                    <input type="text" class="per-acc-main-information-input pa-email repeat" placeholder="Repeat your new email">
                    <div class="per-acc-main-information-changeemail-button">OK</div>
                </div>
                <div class="per-acc-main-information-label">Password:</div>
                <div class="change-wrapper">
                    <input type="password" class="per-acc-main-information-input pa-password" value="Your password">
                    <div class="per-acc-main-information-buttonchange" id="changePassword"></div>
                </div>
                <div class="per-acc-main-information-changepass">
                    <div class="per-acc-main-information-label">Old password:</div>
                    <input type="password" class="per-acc-main-information-input pa-password old" placeholder="Enter your old password">
                    <div class="per-acc-main-information-label">New password:</div>
                    <input type="password" class="per-acc-main-information-input pa-password first" placeholder="Enter your new password">
                    <div class="per-acc-main-information-label">Repeat new password:</div>
                    <input type="password" class="per-acc-main-information-input pa-password repeat" placeholder="Repeat your new password">
                    <div class="per-acc-main-information-changepass-button">OK</div>
                </div>
                <button class="per-acc-main-information-button">Save</button>
            </div>
            <div class="per-acc-main-history" id="paHistory">
                @foreach($orders as $order)
                    <div class="per-acc-main-history-block">
                        <div class="per-acc-main-history-block-title">{{ $order->order_type['translation'][0]->name }}</div>
                        <div class="per-acc-main-history-block-head">
                            <div class="per-acc-main-history-block-head-item"><span class="pa-head-before">Date of payment: </span>&nbsp;{{ $order->payment_date->format('d.m.Y') }}</div>
                            <div class="per-acc-main-history-block-head-item"><span class="pa-head-before">Date of completion: </span>&nbsp;{{ $order->payment_date->addMonth(1)->format('d.m.Y') }}</div>
                            <div class="per-acc-main-history-block-head-item"><span class="pa-head-before">Number of served computers: </span>&nbsp;{{ $order->order_type->computers }}</div>
                        </div>
                        <div class="per-acc-main-history-block-foot">
                            <div class="per-acc-main-history-block-foot-before">Products: </div>
                            <div class="per-acc-main-history-block-foot-items">&nbsp;
                            @foreach($order->computers as $computer)
                                <div class="per-acc-main-history-block-foot-items-item">&nbsp;{{ $computer->code }}</div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection