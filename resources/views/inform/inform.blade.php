@extends('layouts.app')
@section('title', 'TeamViewer')
@section('page', 'other')
@section('content')
    <div class="teamviewer">
        <div class="teamviewer-container">
            <a class="main-nav tv-nav"><a class="tv-link" onclick="return clickHandler()" href="/">Home |</a><a href="#" class="main-nav-green"> TeamViewer</a></a>
            <div class="teamviewer-title">{{ trans('teamviewer.title1') }} <br>{{ trans('teamviewer.title2') }}</div>
        </div>
    </div>
    <div class="teamviewer-main">
        <div class="teamviewer-main-container">
            <div class="teamviewer-main-instruction">
                <div class="imgBg">
                    <img src="" alt="">
                    <div class="imgBg-cross">x</div>
                </div>
                <div class="teamviewer-main-instruction-step">
                    <div class="teamviewer-main-instruction-step-pretitle">Step 1</div>
                    <div class="teamviewer-main-instruction-step-container">
                        <div class="teamviewer-main-instruction-step-title">Open the TeamViewer website and download the program</div>
                        <div class="teamviewer-main-instruction-step-text">Download the program on the TeamViewer website.</div>
                        <div class="teamviewer-main-instruction-step-button">Download</div>
                        <div class="teamviewer-main-instruction-step-text">Select your operating system. If your operating system isn't correctly selected when the website loads, click your system (e.g., Mac) near the top of the page.<br><br>Scroll down and click Download TeamViewer. It's a green button near the middle of the page.Wait for TeamViewer to finish downloading. This should only take a few minutes.</div>
                        <img class="tv-img" src="{{ asset('images/img-tv-1.png') }}" alt="">
                    </div>
                </div>
                <div class="teamviewer-main-instruction-step">
                    <div class="teamviewer-main-instruction-step-pretitle">Step 2</div>
                    <div class="teamviewer-main-instruction-step-container">
                        <div class="teamviewer-main-instruction-step-title">Install TeamViewer</div>
                        <div class="teamviewer-main-instruction-step-text">Double-click the TeamViewer setup file. You'll find this file, which is an EXE for Windows and a DMG for Macs, in your computer's Download folder. The setup window will open.<br><br>Install TeamViewer. Check the "Installation to access this computer remotely" box, check the "Personal / Non-commercial use" box, uncheck the "Show advanced settings" box if necessary, click Accept - finish, and click Yes when prompted.</div>

                        <iframe class="video" src="https://www.youtube.com/embed/efK1oiLYq0w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="teamviewer-main-instruction-step">
                    <div class="teamviewer-main-instruction-step-pretitle">Step 3</div>
                    <div class="teamviewer-main-instruction-step-container">
                        <div class="teamviewer-main-instruction-step-title">Close the "Unattended setup" wizard if prompted and enable the new user interface</div>
                        <div class="teamviewer-main-instruction-step-text">Close the "Unattended setup" wizard if prompted. If this is your first time opening TeamViewer, you'll be prompted to navigate through an "Unattended setup" wizard; if so, just click Cancel.<br><br>Enable the new user interface. Click the Try it now button at the top of the TeamViewer window if it's available. This will ensure that your TeamViewer dashboard is up-to-date.</div>
                    </div>
                </div>
                <div class="teamviewer-main-instruction-step">
                    <div class="teamviewer-main-instruction-step-pretitle">Step 4</div>
                    <div class="teamviewer-main-instruction-step-container">
                        <div class="teamviewer-main-instruction-step-title">Enter the second computer's ID into TeamViewer on the first computer</div>
                        <div class="teamviewer-main-instruction-step-text">Enter the second computer's ID into TeamViewer on the first computer. Type the "ID" number into the "Partner ID" text field on the right side of the TeamViewer window on the computer from which you want to connect.</div>
                        <img class="tv-img" src="{{ asset('images/img-tv-2.png') }}" alt="">
                        <div class="teamviewer-main-instruction-step-text">Click CONNECT. It's a blue button below the text field. Doing so will prompt your computer to begin attempting to connect to the other computer.</div>
                    </div>
                </div>
            </div>
            <div class="teamviewer-main-sidebar">
                <div class="teamviewer-main-sidebar-head">
                    <div class="teamviewer-main-sidebar-head-pretitle">{{ trans('teamviewer.sidebar_title') }}</div>
                    <div class="teamviewer-main-sidebar-head-title">{{ trans('teamviewer.sidebar_subtitle') }}</div>
                </div>
                @foreach($notes as $note)
                    <div class="teamviewer-main-sidebar-advantage" style="background-image: url({{ $note->image }});">
                        <div class="teamviewer-main-sidebar-advantage-title">{{ $note->translation->first()->title }}</div>
                        <div class="teamviewer-main-sidebar-advantage-text">{{ $note->translation->first()->text }}</div>
                    </div>
                    @endforeach
            </div>
        </div>
    </div>
@endsection