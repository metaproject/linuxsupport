<?php
$total=$paginate->total();
$perPage = $paginate->perPage();
$lastPage = ceil($total/$perPage);
$curPage = $paginate->currentPage();
?>
@if($lastPage>1)
    <div class="admin-main-item-orders-slider">
        <?php $url = $paginate->url(1) ?>
        <a href="{{ $url }}"><div class="admin-main-item-orders-slider-item-arrow">&#10094;</div></a>
        <?php $p=1 ?>
        @while($p<=$lastPage)
            <?php $url = $paginate->url($p) ?>
            @if($p>$curPage+3)
                <?php $p=$lastPage ?>
                <div class="admin-main-item-orders-slider-item-dots">...</div>
                <div class="admin-main-item-orders-slider-item">{{ $lastPage }}</div>
            @else
                <a href="{{ $url }}"><div class="admin-main-item-orders-slider-item {{ ($curPage == $p)?'admin-slider-active':'' }}">{{ $p }}</div></a>
            @endif
            @if($p<$curPage-3)
                <div class="admin-main-item-orders-slider-item-dots">...</div>
                <?php $p=$curPage-3; ?>
            @else
                <?php $p++; ?>
            @endif
        @endwhile
        <?php $url = $paginate->url($lastPage) ?>
        <a href="{{ $url }}"><div class="admin-main-item-orders-slider-item-arrow">&#10095;</div></a>

    </div>
@endif