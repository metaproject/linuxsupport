<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Linux Admin {{ $title }}</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body class="admin-body other" style="background-color: #f3f5ff">
<div class="admin">
    <div class="sidebar-wrapper">
    <div class="admin-sidebar">
        <a href="/"><div class="admin-sidebar-logo"></div></a>
        @foreach($menu as $m)
            <a style="text-decoration: none" href="{{ $m->url }}" class="admin-sidebar-item {{ ($title==$m->name)?'admin-sidebar-active':'' }}">{{ trans('menu.'.$m->menu_type_id)[$m->id] }}
                @if($m->name=='Orders'&&isset($new_orders_count)&&$new_orders_count!=0)
                    <div class="admin-sidebar-item-notification">{{ $new_orders_count }}</div>
                    @endif
            </a>
        @endforeach
    </div>
    </div>
    <div class="admin-main">
        <div class="admin-main-head">
            @guest
            @else
            <div class="admin-main-head-name">{{ $user->name }}
                <div class="admin-main-head-name-icon"></div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
                @endguest
        </div>
        @yield('content')

    </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>
</body>
</html>

