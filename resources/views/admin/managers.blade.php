
    @extends('admin.layouts.admin')

@section('content')
    <div class="main">
        <div class="orders">
            <span class="orders-search">Search by manager:</span>
            <input placeholder="manager" type="text">
            <button class="orders-button">Search</button>
            <div></div>
            <span class="orders-search">Search by date:</span>
            <span>From: </span>
            <input type="date">
            <span>To: </span>
            <input type="date">
            <button class="orders-button">Search</button>
            <div></div>
            <button id="addform1" class="orders-button">Add Manager</button>
            <div class="orders-form" id="form1">
                <input type="text" placeholder="Email" id="">
                <input type="text" placeholder="Password" id="">
                <input type="text" placeholder="Name" id="">
                <span>Photo:
                    <input type="file">
                </span>
                <button id="addform1button">Add</button>
            </div>
            <div></div>
            <button id="addform2" class="orders-button">Add Manager from user list</button>
            <div class="orders-form" id="form2">
                <ul>
                    <li>User1</li>
                    <li>User2</li>
                    <li>User3</li>
                    <li>User4</li>
                    <li>User5</li>
                    <li>User6</li>
                    <li>User7</li>
                    <li>User8</li>
                </ul>
                <button id="addform2button">Add</button>
            </div>
            <div class="order">
                <div class="order-delete">Delete</div>
                <div class="order-name">Manager Name</div>
                <ul class="order-orders">
                    <li>Order1</li>
                    <li>Order2</li>
                    <li>Order3</li>
                    <li>Order4</li>
                    <li>Order5</li>
                </ul>
                <ul class="order-orders sub-orders">
                    <li>Sub-Order1</li>
                    <li>Sub-Order2</li>
                    <li>Sub-Order3</li>
                    <li>Sub-Order4</li>
                    <li>Sub-Order5</li>
                </ul>
            </div>
            <div class="order">
                <div class="order-delete">Delete</div>
                <div class="order-name">Manager Name</div>
                <ul class="order-orders">
                    <li>Order1</li>
                    <li>Order2</li>
                    <li>Order3</li>
                    <li>Order4</li>
                    <li>Order5</li>
                </ul>
                <ul class="order-orders sub-orders">
                    <li>Sub-Order1</li>
                    <li>Sub-Order2</li>
                    <li>Sub-Order3</li>
                    <li>Sub-Order4</li>
                    <li>Sub-Order5</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
    <script>
        window.onload = function() {
            $('.table tbody tr').click(function () {
                var id = $(this).data('id');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    data:{manager:id,sections:['managers']},
                    url: '/admin/search',
                    success: function (data) {
                        $('.results-text').html('');
                        if ('orders' in data) {
                            var string='<strong>{{ trans('forms/admin_home_result.orders_title') }}</strong><table class="table table-striped"><thead><tr><th>№</th><th>{{ trans('forms/admin_home_result.orders_1') }}</th><th>{{ trans('forms/admin_home_result.orders_2') }}</th><th>{{ trans('forms/admin_home_result.orders_3') }}</th> <th>{{ trans('forms/admin_home_result.orders_4') }}</th><th>{{ trans('forms/admin_home_result.orders_5') }}</th></tr> </thead><tbody>';

                            $.each(data['orders'].data, function (index, value) {
                                string+='<tr><td>'+value.id+'</td><td>'+value.client.user.name+'</td><td>'+value.order_type.name+'</td><td>'+value.order_status.name+'</td><td>'+value.payment_date+'</td><td>'+value.expire_date+'</td></tr>';
                            });
                            $('.results-text').append(string);
                        }
                    }
                });

            });
        }
    </script>