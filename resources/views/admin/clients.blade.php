@extends('admin.layouts.admin')

@section('content')
    <div class="main">
        <div class="orders">
            @foreach($clients as $client)
                <div class="order">
                    <div class="order-id">{{ $client->id }}</div>
                    <div class="order-name">{{ $client->user->name }}</div>
                    @foreach($client->user->phone as $phone)
                    <div class="order-number">{{ $phone->coordinate }}</div>
                    @endforeach
                    <div class="order-communication">{{ $client->communication->name }}</div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
