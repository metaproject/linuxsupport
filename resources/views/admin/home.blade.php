@extends('admin.layouts.admin')

@section('content')
    <div class="main">
        <form class="form" action="" method="get">
            <input type="tel" id="phone" name="phone" placeholder="123-456-7890" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" />
            <input placeholder="Name" type="text">
            <input type="date">
            <input placeholder="Order number" type="text">
            <input placeholder="Status" type="text">
            <button type="submit">Поиск</button>
        </form>
        <div class="text">
            <div class="text-text">
            </div>
        </div>
    </div>
@endsection
<script>
    window.onload = function() {
        $('button[type="submit"]').click(function (e) {
            e.preventDefault();
            var Phone_number = $('form [name="phone"]').val(),
                Name = $('form [placeholder="Name"]').val(),
                Date = $('form [type="date"]').val(),
                Order_number = $('form [placeholder="Order number"]').val(),
                Status = $('form [placeholder="Status"]').val();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    data:{name:Name,phone:Phone_number,pay_date:Date,order:Order_number,status:Status,sections:['orders','managers','clients']},
                    url: '/admin/search',
                    success: function (data) {
                        $('.text-text').html('');
                        if ('orders' in data) {
                            var string='<strong>{{ trans('forms/admin_home_result.orders_title') }}</strong><table class="table table-striped"><thead><tr><th>№</th><th>{{ trans('forms/admin_home_result.orders_1') }}</th><th>{{ trans('forms/admin_home_result.orders_2') }}</th><th>{{ trans('forms/admin_home_result.orders_3') }}</th> <th>{{ trans('forms/admin_home_result.orders_4') }}</th><th>{{ trans('forms/admin_home_result.orders_5') }}</th></tr> </thead><tbody>';

                            $.each(data['orders'].data, function (index, value) {
                                string+='<tr><td>'+value.id+'</td><td>'+value.client.user.name+'</td><td>'+value.order_type.name+'</td><td>'+value.order_status.name+'</td><td>'+value.payment_date+'</td><td>'+value.expire_date+'</td></tr>';
                            });
                            $('.text-text').append(string);
                        }
                        if ('clients' in data) {
                            string='<strong>{{ trans('forms/admin_home_result.clients_title') }}</strong><table class="table table-bordered"><thead><tr><th>{{ trans('forms/admin_home_result.clients_1') }}</th><th>{{ trans('forms/admin_home_result.clients_2') }}</th><th>{{ trans('forms/admin_home_result.clients_3') }}</th> </tr> </thead><tbody>';

                            $.each(data['clients'].data, function (index, value) {
                                string+='<tr><td>'+value.user.name+'</td><td>'+value.communication.name+' '+value.preferCoordinate+'</td><td>'+value.created_at+'</td></tr>';
                            });
                            $('.text-text').append(string);
                        }
                        if ('managers' in data) {
                            string='<strong>{{ trans('forms/admin_home_result.managers_title') }}</strong><table class="table table-striped"><thead><tr><th>{{ trans('forms/admin_home_result.managers_1') }}</th><th>{{ trans('forms/admin_home_result.managers_2') }}</th></tr> </thead><tbody>';

                            $.each(data['managers'].data, function (index, value) {
                                string+='<tr><td>'+value.user.name+'</td><td>'+value.user.email+'</td></tr>';
                            });
                            $('.text-text').append(string);
                        }

                    }
                });

        });
    }
</script>
