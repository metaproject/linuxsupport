@extends('admin.layouts.admin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @foreach($notes as $note)
                            <div class="alert alert-success" role="alert">
                                {{ $note }}
                            </div>
                        @endforeach

                    </div>
                    <div class="card-body">
                        @foreach($note_types as $note_type)
                            <div class="alert alert-success" role="alert">
                                {{ $note_type }}
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
