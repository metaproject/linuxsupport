@extends('admin.layouts.admin')

@section('content')
    <div class="admin-main-item">
        <div class="admin-main-item-head">Orders</div>
        <div class="admin-main-item-orders">
            <div class="admin-main-item-orders-container">
                <div class="admin-main-item-orders-header">
                    <div class="admin-main-item-orders-header-search-wrapper">
                    <input type="text" class="admin-main-item-orders-header-search" placeholder="Search order ID / client">
                        <button class="admin-main-item-orders-header-search-button"></button>
                    </div>
                    <div class="admin-main-item-orders-header-right">
                        <input type="text" id="datepicker" {{--onfocus="(this.type='date')"--}} placeholder="From" class="admin-main-item-orders-header-date"><span class="admin-main-item-orders-header-date-between">-</span>
                        <input type="text" id="datepicker2" {{--onfocus="(this.type='date')"--}} placeholder="To" class="admin-main-item-orders-header-date">
                        <div class="select-wrapper">
                            <div class="admin-select-placeholder">Status:</div>
                            <select name="statuses" class="admin-main-item-orders-header-select">
                                <option value="">All</option>
                                @foreach($statuses as $status)
                                    <option value="" data-id="{{ $status->id }}">{{ $status->name }}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="admin-main-item-orders-content">
                    <div class="admin-main-item-orders-content-header">
                        <div class="admin-main-item-orders-content-header-item">ID</div>
                        <div class="admin-main-item-orders-content-header-item">Date</div>
                        <div class="admin-main-item-orders-content-header-item">Client</div>
                        <div class="admin-main-item-orders-content-header-item">Phone</div>
                        <div class="admin-main-item-orders-content-header-item">Plan</div>
                        <div class="admin-main-item-orders-content-header-item">Status</div>
                        <div class="admin-main-item-orders-content-header-item">Manager</div>
                    </div>
                    @foreach($orders as $order)
                        <div class="admin-main-item-orders-content-item">
                            <div class="admin-main-item-orders-content-item-item">{{ $order->id }}</div>
                            <div class="admin-main-item-orders-content-item-item">{{ $order->payment_date->format('d.m.Y') }}</div>
                            <div class="admin-main-item-orders-content-item-item">{{ $order->client->user->name }}</div>
                            <div class="admin-main-item-orders-content-item-item admin-phone">
                                <div class="admin-phone-item">{{ $order->client->user->phone[0]->coordinate }}</div>
                                @foreach($order->client->user->communication as $com)
                                <img class="admin-phone-item" title="{{ $com->coordinate }}" src="{{ asset($com->communication->image) }}" width="17px" height="17px">
                                    @endforeach
                            </div>
                            <div class="admin-main-item-orders-content-item-item">{{ $order->order_type->translation[$lang->first()->id-1]->name }}</div>
                            <div class="admin-main-item-orders-content-item-item {{ ($order->order_status->id==2)?'item-processed':'' }} admin-item-status">{{ $order->order_status->name }}</div>
                            <div class="admin-main-item-orders-content-item-item">{{ ($order->manager_id)?$order->manager->user->name:'' }}</div>
                            @if($role=='manager')
                                @if(!$order->manager_id)
                                <div class="admin-main-item-orders-content-item-item admin-item-button" data-href="admin/orders/take/{{ $order->id }}">Take it</div>
                                    @else
                                <div class="admin-main-item-orders-content-item-item admin-item-icons">
                                    <div class="admin-item-icons-icon"></div>
                                    <div class="admin-item-icons-icon"></div>
                                    <div class="admin-item-icons-icon"></div>
                                </div>
                                    @endif
                            @else

                                @endif
                        </div>
                        @endforeach
                </div>
                @include('admin.layouts.paginate',['paginate'=>$orders])
            </div>
        </div>
    </div>
@endsection
