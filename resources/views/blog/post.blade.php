@extends('layouts.app')
@section('title', 'Blog Post')
@section('page', 'other')
@section('content')
    <div class="main">
        <div class="main-container">
            <a><a onclick="return clickHandler()" class="main-nav" href="/"> {{ trans('breadcrumbs.home') }} |</a> <a  class="main-nav-item" href="/blog">{{ trans('blog.title') }} |</a> <a href="#" class="main-nav-green"> {{ $article->translation[0]->title }}</a></a>
            <a href="/blog" style="text-decoration: none" class="main-prev">
                <div class="main-prev-icon">&lt;</div>
                <div class="main-prev-text">{{ trans('blog.post_back') }}</div>
            </a>

            <div class="main-posts">
                <div class="main-posts-post">
                    <div class="main-posts-post-title title1">{{ $article->translation[0]->title }}</div>
                    <img src="{{ asset($article->image) }}" alt="">
                    <div class="main-posts-post-text text1">{{ $article->translation[0]->text }}</div>
                    @if(count($article->tags)>0)
                    <div class="main-post-tags">
                        @foreach($article->tags as $tag)
                        #{{$tag->name}}
                            @endforeach
                    </div>
                        @endif
                </div>
                <div class="main-posts-sidebar">
                    <div class="main-posts-sidebar-title">{{ trans('blog.post_sidebar_title') }}</div>
                    @foreach($articles as $art)
                        <a href="/blog/{{ $art->id }}">
                            <div class="main-posts-sidebar-post">
                                <img src="{{ asset($art->image) }}" alt="">
                                <div class="main-posts-sidebar-post-text">{{ $art->translation[0]->title }}</div>
                            </div>
                        </a>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection