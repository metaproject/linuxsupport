@extends('layouts.app')
@section('title', 'Blog')
@section('page', 'other')
@section('content')
    <div class="main blog_main">
        <div class="main-container">
            <a><a  class="main-nav" style="text-decoration: none;color: rgba(126, 128, 144, 0.8);" onclick="return clickHandler()" href="/"> {{ trans('breadcrumbs.home') }} |</a><a style="text-decoration: none" href="/blog" class="main-nav-green"> {{ trans('blog.title') }}</a></a>
            <div class="articles-container" style="margin-top: 83px">
                <span class="main-title">{{ trans('blog.title') }}</span>
                <span class="main-search" id="search">
                    <input v-model="string" placeholder="{{ trans('blog.search_placeholder') }}" type="text">
                    <div v-on:click="search" class="main-search-icon"></div>
                </span>
                <div class="main-articles">
                    @foreach($articles as $article)
                        <a href="/blog/{{ $article->id }}">
                    <div class="main-articles-article">
                        <img width="360px" src="{{ asset($article->image) }}" alt="img-1">
                        <div class="main-articles-article-title">{{ $article->translation[0]->title }}</div>
                        <div class="main-articles-article-text">{{ str_limit($article->translation[0]->text, $limit = 300, $end = '...') }}</div>
                    </div>
                        </a>
                        @endforeach
                </div>
                @include('layouts.paginate',['paginate'=>$articles])
            </div>
        </div>
    </div>
@endsection