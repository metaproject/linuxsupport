<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Linux Blog</title>
    <link rel="stylesheet" href="{{ asset('css/blog.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script>//errors
        var first_form_phone='{{ trans('messages.first_form_phone') }}',
            first_form_com='{{ trans('messages.first_form_com') }}';
    </script>
</head>
<body style="opacity:0;transition: opacity .2s;">
@include('popups.home')
<div class="header">
    <div class="container">
        <a class="header-logo" onclick="return clickHandler()" href="/"><span class="header-logo-bold">LINUX</span>Support</a>
        <div class="header-right">
            <div class="header-right-phone" id="logIn">{{ $custom_fields['our_phone']->value }}</div>
            <div class="header-right-lang-wrapper">
                <div class="header-right-lang-wrapper-lang" id="lang">{{ strtoupper($lang->first()->name) }}</div>
                <div class="ru"><a href="/setlocale/{{ $lang->get(1)->name }}">{{ strtoupper($lang->get(1)->name) }}</a></div>
            </div>
            <div class="header-right-phone-mobile"></div>
            @guest
            <div class="header-right-phone-mobile"></div>
            <button class="header-right-button logIn">{{ trans('homepage.buttons')['header_login'] }}</button>
            <div class="header-right-button-mobile"></div>
        @else
                <div class="header-right-button-mobile"></div>
                @endguest
        </div>
    </div>
</div>
@yield('content')
<div class="footer">
    <div class="footer-line"></div>
    <div class="footer-container">
        <div class="footer-top">
            <div class="footer-top-title">{{ trans('homepage.footer_subtitle') }}<div class="footer-top-title-bold">{{ trans('homepage.footer_title') }}</div></div>
            <div class="footer-top-button popupShow">{{ trans('homepage.buttons')['bottom_button1'] }}</div>
        </div>
        <div class="footer-bottom">
            <div class="footer-bottom-phone">{{ $custom_fields['our_phone']->value }}</div>
            <div class="footer-bottom-copyright">© LinuxSupport. {{ trans('homepage.footer_rights') }}</div>
        </div>
    </div>
</div>
<script src="{{ asset('js/app.js') }}" defer></script>



<script src="{{ asset('js/popup(s).js') }}" defer></script>
</body>
</html>