@extends('layouts.app')
@section('title', 'Payment')
@section('page', 'other')
{{ print_r(session('order')) }}
@section('content')
    <div class="teamviewer payment">
        <div class="teamviewer-container">
            <a class="main-nav tv-nav"><a class="tv-link" onclick="return clickHandler()" href="index.html">Home |</a><a href="payment.html" class="main-nav-green"> {{ trans('payment.title') }}</a></a>
            <div class="teamviewer-title">{{ trans('payment.title') }}<br>LinuxSupport</div>
        </div>
    </div>
    <div class="payment-main">
        <div class="payment-main-container">
            <div class="payment-main-pay">
                <div class="payment-main-pay-title">{{ trans('payment.pay_title') }}</div>
                <form id="form" action="" method="POST" {{--onsubmit=" thanksScript();return false;"--}}>
                <div class="payment-main-pay-label">{{ trans('payment.name_title') }}</div>
                <input required type="text" placeholder="{{ trans('payment.name_placeholder') }}" value="{{ Auth::user()->name }}" class="payment-main-pay-input pay-name">
                <div class="payment-main-pay-label">{{ trans('payment.phone_title') }}</div>
                <input required type="text" placeholder="{{ trans('payment.phone_placeholder') }}" value="{{ $session['client']['phone'] }}" class="payment-main-pay-input pay-phone">
                <div class="head-main-right-icons">
                    <div class="head-main-right-icons-icon tl {{ ($session['client']['prefer']==1)?'tlc':'' }}" title="Telegram" style=""></div>
                    <div class="head-main-right-icons-icon wh {{ ($session['client']['prefer']==2)?'whc':'' }}" title="Whatsapp"></div>
                    <div class="head-main-right-icons-icon vb {{ ($session['client']['prefer']==3)?'vbc':'' }}" title="Viber"></div>
                    <div class="head-main-right-icons-icon sk {{ ($session['client']['prefer']==4)?'skc':'' }}" title="Skype"></div>
                    <div class="head-main-right-icons-icon em {{ ($session['client']['prefer']==5)?'emc':'' }}" title="Email"></div>
                    <div class="head-main-right-icons-icon cl {{ ($session['client']['prefer']==6)?'clc':'' }}" title="Call"></div>
                </div>
                <div class="payment-main-pay-label">{{ trans('payment.payment_title') }}</div>
                <select class="payment-main-pay-input pay-select">
                    <option value="Stripe">Stripe</option>
                    <option value="Stripe">Stripe</option>
                    <option value="Stripe">Stripe</option>
                </select>
                <div class="payment-main-pay-label">{{ trans('payment.card_title') }}</div>
                <input required type="text" placeholder="{{ trans('payment.card_placeholder') }}" class="payment-main-pay-input card_number">
                <div class="payment-main-pay-inputs">
                    <div class="payment-main-pay-inputs-col">
                        <div class="payment-main-pay-label">{{ trans('payment.date_title') }}</div>
                        <div class="payment-main-pay-inputs-col-col">
                            <input required type="text" class="payment-main-pay-input col-input month">
                            <div class="col-line">/</div>
                            <input required type="text" class="payment-main-pay-input col-input year">
                        </div>
                    </div>
                    <div class="payment-main-pay-inputs-col2">
                        <div class="payment-main-pay-label">CVC:</div>
                        <input required type="text" class="payment-main-pay-input">
                    </div>
                </div>
                    <input type="submit" style="border: none; display: block" class="payment-main-pay-button" value="{{ trans('payment.button') }}">
                </form>
            </div>
            <div class="payment-main-sidebar">
                <div class="payment-main-sidebar-head">{{ trans('payment.inform_title') }}</div>
                <div class="payment-main-sidebar-title">{{ trans('payment.inform_subtitle') }}</div>
                <div class="payment-main-sidebar-columns">
                    <div class="payment-main-sidebar-columns-col">
                        <div class="payment-main-sidebar-columns-col-item">{{ trans('payment.inform_plan') }}</div>
                        <div class="payment-main-sidebar-columns-col-item">{{ trans('payment.inform_computers') }}</div>
                        <div class="payment-main-sidebar-columns-col-item">{{ trans('payment.inform_price') }}</div>
                    </div>
                    <div class="payment-main-sidebar-columns-col2">
                        <div class="payment-main-sidebar-columns-col2-item">{{ $order_type->translation[$lang->first()->id-1]->name }}</div>
                        <div class="payment-main-sidebar-columns-col2-item">{{ $order_type->computers }} {{ trans_choice('payment.inform_compCount',$order_type->computers) }}</div>
                        <div class="payment-main-sidebar-columns-col2-item">${{ (array_key_exists('cost',$session['order']))?$session['order']['cost']:$order_type->cost }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection