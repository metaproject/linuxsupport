<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'ActivatedSuccess'=>'Your account activated successfully',
    'ActivationAccount'=>'LinuxSupport account activation',
    'ActivatedWrong'=>'Activation wrong',
    'ActivatedAlready'=>'Your account already activated',
    'Activate'=>'Registration completed successfully. We sent letter on your e-mail to activate your account.',
    'ActivatePlease1'=>'Your account has not been activated. Activate it by going to the post in the letter sent to you. If the letter does not come, ',
    'ActivatePlease2'=>'we will send you one more',
    'ActivatePlease3'=>'Your account has not been activated. Activate it by going to the post in the letter sent to you.',
    'first_title' => 'We`ll solve any problem with your Linux',
    'first_form_phone'=>'Write your phone,please',
    'first_form_com'=>'Chose your prefer communication type',
    'register_no_name'=>'Please, write your name',
    'register_no_email'=>'Please, write your email',
    'register_no_password'=>'Please, write your password',
    'register_not_matched'=>'Your passwords do not match',

];
