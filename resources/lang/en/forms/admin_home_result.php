<?php
return [
        'orders_title'=>'Orders',
        'orders_1'=>'Client',
        'orders_2'=>'Type',
        'orders_3'=>'Status',
        'orders_4'=>'Payment date',
        'orders_5'=>'Expired date',
        'clients_title'=>'Clients',
        'clients_1'=>'Name',
        'clients_2'=>'Prefer Communication Channel',
        'clients_3'=>'First order',
        'managers_title'=>'Managers',
        'managers_1'=>'Name',
        'managers_2'=>'Email',
];