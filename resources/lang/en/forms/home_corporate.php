<?php
return [
    'title'=>'Corporate monthly support',
    'li1'=>'Personal support',
    'li2'=>'24/7 support',
    'li3'=>'For :number computer',
    'li4'=>'Only $:cost',
];