<?php
return [
    'title' => 'Log in',
    'email_title' => 'Email:',
    'email_placeholder' => 'Enter your email',
    'password_title' => 'Password:',
    'password_placeholder' => 'Enter your password',
    'forgot_title'=>'Forgot your password?',
    'button' => 'Log in',
    'soc_title' => 'or use account',
    'bottom1' => 'Do you have an account?',
    'bottom2' => 'Sign up'
];