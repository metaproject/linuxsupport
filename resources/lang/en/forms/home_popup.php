<?php
return [
    'title' => 'Request help now',
    'name_title' => 'Name:',
    'name_placeholder' => 'Enter your name',
    'phone_title' => 'Contact Number:',
    'phone_placeholder' => 'Enter a phone number',
    'plan_title'=>'Choose your plan:',
    'type1' => 'One Time Help',
    'type2' => 'Monthly Help',
    'type3' => 'Corporate monthly support',
    'descr_title'=>'Your problem:',
    'descr_placeholder' => 'Describe your problem here',
    'button' => 'Send',
];