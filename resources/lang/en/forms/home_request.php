<?php
return [
        'title' => 'Request help now',
        'phone_title' => 'Contact Number:',
        'phone_placeholder' => 'Enter a phone number',
        'type1' => 'One Time help for $:cost',
        'type2' => 'Monthly help for $:cost',
        'type3' => 'Corporate Monthly Support for $:cost',
        'button' => 'Request Linux Help',
];