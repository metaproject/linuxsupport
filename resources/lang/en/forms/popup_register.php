<?php
return [
    'title' => 'Sign up',
    'name_title' => 'Name:',
    'name_placeholder' => 'Enter your name',
    'email_title' => 'Email:',
    'email_placeholder' => 'Enter your email',
    'password_title' => 'Password:',
    'password_placeholder' => 'Enter your password',
    'button' => 'Sign up',
    'soc_title' => 'or use account',
    'bottom1' => 'Do you have an account?',
    'bottom2' => 'Log in'
];