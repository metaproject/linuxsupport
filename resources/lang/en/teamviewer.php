<?php
return [
    'title1' => 'How to provide',
    'title2' => 'TeamViewer access?',
    'sidebar_title' => 'Features TeamViewer',
    'sidebar_subtitle' => 'Functionality that Makes It Easy to Work Remotely'
];