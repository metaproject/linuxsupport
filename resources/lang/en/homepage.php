<?php
return [
    'buttons' => [
        'header_login' => 'Login for customers',
        'first_button1' => 'How it works?',
        'second_button1' => 'How to provide TeamViewer access?',
        'second_button2' => 'Get support',
        'third_button1' => 'Request Linux Help',
        'third_button2' => 'Request Help',
        'fifth_button1' => 'More information',
        'bottom_button1' => 'Get support now'
    ],
    'first_title' => 'We`ll solve any problem with your Linux',
    'first_text' => '24x7 Linux support. No need to waste your time and nerves. Request help and receive solution in minutes.',
    'second_title1' => 'How it Works?',
    'second_subtitle1' => 'How it Works?',
        'second_note_footnote' => 'Detailed instruction',
    'second_title2' => 'A few reasons why we might be the right fit',
    'second_subtitle2' => 'Our advantages',
    'third_title2' => 'Cost of Linux Support Services',
    'third_subtitle2' => 'Cost of Services',
    'fourth_title' => 'Our clients trust us',
    'fourth_subtitle' => 'Testimonials',
    'fifth_title' => 'The Knowledge Base',
    'fifth_subtitle' => 'Blog',
    'sixth_title' => 'Frequently Asked Questions',
    'sixth_subtitle' => 'FAQ',
    'footer_title' => 'Ready to get support?',
    'footer_subtitle' => 'Get in touch',
    'footer_rights' => 'All rights reserved',



];