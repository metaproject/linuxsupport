<?php
return [
    'title'=>'Payment',
    'pay_title'=>'Payment Method',
    'name_title'=>'Name:',
    'name_placeholder'=>'Enter your name',
    'phone_title'=>'Contact Number:',
    'phone_placeholder'=>'Enter your phone number',
    'payment_title'=>'Payment:',
    'card_title'=>'Number Card:',
    'card_placeholder'=>'Enter your card number',
    'date_title'=>'Expiry Date:',
    'button'=>'Pay',
    'inform_title'=>'Payment information',
    'inform_subtitle'=>'Receive help with unlimited issues listed with your request',
    'inform_plan'=>'Plan',
    'inform_computers'=>'Number of served computers',
    'inform_compCount'=>'computer|computers',
    'inform_price'=>'Price'
];