<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'first_title' => 'Решаем любые проблемы вашего Линукса',
    'first_form_phone'=>'Укажите ваш номер телефона',
    'first_form_com'=>'Выберите предпочтительный тип связи',
    'ActivatedSuccess'=>'Ваш аккаунт успешно активирован',
    'ActivationAccount'=>'LinuxSupport активация аккаунта',
    'ActivatedWrong'=>'Активация не удалась',
    'ActivatedAlready'=>'Ваш аккаунт уже был активирован',
    'Activate'=>'Регистрация прошла успешно. На ваш e-mail отправленно письмо для активации аккаунта',
    'ActivatePlease1'=>'Ваш аккаунт не активирован. Активируйте его пройдя поссылке в отправленном вам письме. Если письмо не пришло, ',
    'ActivatePlease2'=>'мы вышлем вам еще одно',
    'ActivatePlease3'=>'Ваш аккаунт не активирован. Активируйте его пройдя поссылке в отправленном вам письме.',
    'register_no_name'=>'Пожалуйста, введите ваше имя',
    'register_no_email'=>'Пожалуйста, введите ваш email',
    'register_no_password'=>'Пожалуйста, укажите ваш пароль',
    'register_not_matched'=>'Пароли не совпадают',

];
