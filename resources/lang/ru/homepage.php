<?php
return [
    'buttons' => [
        'header_login' => 'Логин для клиентов',
        'first_button1' => 'Как это работает?',
        'second_button1' => 'Дать доступ через TeamViewer?',
        'second_button2' => 'Получить поддержку',
        'third_button1' => 'Запросить помощь',
        'third_button2' => 'Запросить помощь',
        'fifth_button1' => 'Больше информации',
        'bottom_button1' => 'Получить помощь сейчас'
    ],
    'first_title' => 'Мы решим любую проблему вашего Линукса',
    'first_text' => '24x7 поддержка Линукса. Нет нужды тратить ваше время и деньги. Запросите поддержку и получите решение вашей проблемы за минуту.',
    'second_title1' => 'Как это работает?',
    'second_subtitle1' => 'Как это работает?',
    'second_note_footnote' => 'Детальная инструкция',
    'second_title2' => 'Несколько причин почему мы являемся хорошим выбором',
    'second_subtitle2' => 'Наши преимущества',
    'third_title2' => 'Стоимость услуг Линукс поддержки',
    'third_subtitle2' => 'Стоимость услуг',
    'fourth_title' => 'Наши клиенты доверяют нам',
    'fourth_subtitle' => 'Отзывы',
    'fifth_title' => 'База знаний',
    'fifth_subtitle' => 'Блог',
    'sixth_title' => 'Часто задаваемые вопросы',
    'sixth_subtitle' => 'FAQ',
    'footer_title' => 'Готовы получить помощь?',
    'footer_subtitle' => 'Связаться',
    'footer_rights' => 'Все права защищены',



];