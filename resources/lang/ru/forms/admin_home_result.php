<?php
return [
        'orders_title'=>'Заказы',
        'orders_1'=>'Клиенты',
        'orders_2'=>'Тип',
        'orders_3'=>'Статус',
        'orders_4'=>'Дата оплаты',
        'orders_5'=>'Дата окончания',
        'clients_title'=>'Клиенты',
        'clients_1'=>'Имя',
        'clients_2'=>'Предпочитаемый канал связи',
        'clients_3'=>'Первый заказ',
        'managers_title'=>'Менеджеры',
        'managers_1'=>'Имя',
        'managers_2'=>'Email',
];