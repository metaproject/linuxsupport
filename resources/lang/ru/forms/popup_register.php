<?php
return [
    'title' => 'Регистрация',
    'name_title' => 'Имя:',
    'name_placeholder' => 'Введите ваше имя',
    'email_title' => 'Email:',
    'email_placeholder' => 'Введите ваш email',
    'password_title' => 'Пароль:',
    'password_placeholder' => 'Введите ваш пароль',
    'button' => 'Регистрация',
    'soc_title' => 'или используйте другой аккаунт',
    'bottom1' => 'У вас есть аккаунт?',
    'bottom2' => 'Зарегистрируйтесь'
];