<?php
return [
    'title' => 'Вход',
    'email_title' => 'Email:',
    'email_placeholder' => 'Введите ваш email',
    'password_title' => 'Пароль:',
    'password_placeholder' => 'Введите ваш password',
    'forgot_title'=>'Забыли ваш пароль?',
    'button' => 'Вход',
    'soc_title' => 'или используйте другой аккаунт',
    'bottom1' => 'У вас есть аккаунт?',
    'bottom2' => 'Зарегистрируйтесь'
];