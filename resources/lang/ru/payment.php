<?php
return [
    'title'=>'Оплата',
    'pay_title'=>'Способ Оплаты',
    'name_title'=>'Имя:',
    'name_placeholder'=>'Введите ваше имя',
    'phone_title'=>'Контактный телефон:',
    'phone_placeholder'=>'Введите ваш номер телефона',
    'payment_title'=>'Оплата:',
    'card_title'=>'Номер карты:',
    'card_placeholder'=>'Введите номер вашей карты',
    'date_title'=>'Дата окончания:',
    'button'=>'Оплатить',
    'inform_title'=>'Информация по оплате',
    'inform_subtitle'=>'Получите помощь с любым колличеством проблем указанных в вашем запросе',
    'inform_plan'=>'План',
    'inform_computers'=>'Колличество компьютеров',
    'inform_compCount'=>'копьютер|копьютеров',
    'inform_price'=>'Цена'
];