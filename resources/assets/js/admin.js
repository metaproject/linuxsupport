
$('.admin-blog-scroll').each(function(){
  $(this).click(function() { 
    $('body,html').animate({
        scrollTop : 1200 
    }, 300);
  });
})
try {
  $( function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
  $( function() {
    $( "#datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
} catch(er) {
  

}
if(document.title === "Linux Admin Archive"){
  var itemList = document.querySelectorAll('.admin-item-list');
  var arrItemList = Array.prototype.slice.call(itemList);
  arrItemList.forEach(function(e){
      e.addEventListener('click', function(event){
          var currentTop = 0;
          var arrItemListChildren = Array.prototype.slice.call(e.children);
          arrItemListChildren.forEach(function(el){
              el.style.top = currentTop + 'px';
              arrItemListChildren[arrItemListChildren.length - 1].style.borderBottom = '1px solid #ccc'
              el.classList.toggle('admin-none');
              currentTop += 40;
          })
      })
  });
  var itemComments = document.querySelectorAll('.admin-item-comments');
  var arrItemComments = Array.prototype.slice.call(itemComments);
  arrItemComments.forEach(function(elem){
      elem.addEventListener('click', function(){
          var arrItemCommentsChildren = Array.prototype.slice.call(elem.children);
          arrItemCommentsChildren.forEach(function(element){
              element.classList.toggle('admin-none');
          })
      })
  })
} else if(document.title === "Linux Admin Blog") {

try {
  var editor = new Jodit(document.querySelector('#createTitle'));
  document.querySelector('#addTitle').addEventListener('click', function(){
    var newTitle = document.querySelector('#createTitle').cloneNode(true);
    document.querySelector('.admin-main-item-orders-blog-create-main').appendChild(newTitle)
    var editor = new Jodit(newTitle);
  });
  
  var editor = new Jodit(document.querySelector('#createTitleRu'));
  document.querySelector('#addTitleRu').addEventListener('click', function(){
    var newTitleRu = document.querySelector('#createTitleRu').cloneNode(true);
    document.querySelector('.create-ru').appendChild(newTitleRu)
    var editor = new Jodit(newTitleRu);
  });
  // document.querySelector('#addText').addEventListener('click', function(){
  //   var newText = document.querySelector('#createText').cloneNode(true);
  //   document.querySelector('.admin-main-item-orders-blog-create-main').appendChild(newText)
  //   var editor = new Jodit(newText);
  // });
  // document.querySelector('#addTag').addEventListener('click', function(){
  //   var newTag = document.querySelector('#createTag').cloneNode(true);
  //   document.querySelector('.admin-main-item-orders-blog-create-main').appendChild(newTag)
  // });
  // document.querySelector('#addImage').addEventListener('click', function(){
  //   var newImg = document.querySelector('#createImg').cloneNode(true);
  //   document.querySelector('.admin-main-item-orders-blog-create-main').appendChild(newImg)
  // });
  document.querySelector('#addVideo').addEventListener('click', function(){
    var newVideo = document.querySelector('#createVideo');
    newVideo.style.display = 'flex';
    document.querySelector('.admin-main-item-orders-blog-create-main').appendChild(newVideo)
  });
  document.querySelector('#addVideoRu').addEventListener('click', function(){
    var newVideo = document.querySelector('#createVideoRu');
    newVideo.style.display = 'flex';
    document.querySelector('.create-ru').appendChild(newVideo)
  });

  
// var timerId = setInterval(function() {
//   try {
//     document.querySelectorAll('.admin-create-img-img').forEach(function(e, i){
//       e.src = 'images/' + document.querySelectorAll('.inputImg')[i].files[0].name
//     });
//   } catch(err) {

//   }
// }, 1000);
document.querySelector('#create-main-ru').style.display = 'none';
var radio1 = document.querySelector('#contactChoice1');
var radio2 = document.querySelector('#contactChoice2');
radio1.addEventListener('click', function(){
  radio2.checked = false;
  radio1.checked = true;
  document.querySelector('#create-main-ru').style.display = 'none';
  document.querySelector('#create-main-en').style.display = 'flex';
})
radio2.addEventListener('click', function(){
  radio2.checked = true;
  radio1.checked = false;
  document.querySelector('#create-main-ru').style.display = 'flex';
  document.querySelector('#create-main-en').style.display = 'none';
})

} catch(e) {

}

try {
  var iconEdit = document.querySelectorAll('.icon-edit');
var editableContent = document.querySelectorAll('.editableContent');
iconEdit.forEach(function(e, i){
  e.addEventListener('click', function(){
    var editor = new Jodit(editableContent[i]);
  })
})

var iconSrc = document.querySelector('.icon-src');
iconSrc.addEventListener('click', function(){
  document.getElementById('file-input').click();

  var timer = setInterval(function() {
    try {
      document.getElementById('edit-img').src = 'images/' + document.getElementById('file-input').files[0].name
    } catch(err) {

    }
  }, 500)
});
} catch(er) {
  
}
}