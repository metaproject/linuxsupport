if($('body').is('.admin-body')) {
    var status_id;
    $('select[name="statuses"]').change(function(){
        status_id=$(this).children(":selected").data('id');
    });
    $('').find('option').click(function () {
        status_id = $(this).data('id');
    });
    $('.admin-main-item-orders-content-item .admin-main-item-orders-content-item-item.admin-item-button').click(function () {
        window.location = $(this).data('href');
    });
    $('.admin-main-item-orders-header-search-button').click(function () {
        var string =$('input.admin-main-item-orders-header-search').val();
       if($.isNumeric(string)){
           var id=string;
       }else{
           var client=string;
       }
        var from_date=$('#datepicker').val(),
            to_date=$('#datepicker2').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            data: {order:id,name:client,pay_date_first:from_date,pay_date_last:to_date, status:status_id,sections:['orders']},
            url: '/admin/search',
            success: function (data) {
                if(data) {
                    window.location = '/admin/orders/search/'+JSON.stringify(data.orders);
                }
            },
            error:function () {
                    window.location = '/admin';
            }

        });
    });
    $('.admin-main-head-name-icon').click(function () {
        $('#logout-form').submit();
    });
}
if($('.teamviewer').is('.payment')){
    var paymentform = debounce(function () {
        var method=$('.payment-main-pay select.pay-select option:selected').text();
        var card_number=$('.payment-main-pay .card_number').val(),
            cvv=$('.payment-main-pay-inputs-col2 .payment-main-pay-input').val(),
            exp_month=$('.payment-main-pay-inputs-col .payment-main-pay-input.month').val();
        exp_year=$('.payment-main-pay-inputs-col .payment-main-pay-input.year').val();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            data: {card_number:card_number,cvv:cvv,exp_month:exp_month,exp_year:exp_year},
            url: '/payment/'+method,
            success: function (data) {
                thanksScript();
            }

        });
    });
    $('.payment-main-pay').on('submit', 'form', function(e){
        e.preventDefault();
        paymentform();
    });
}
if($('.teamviewer').is('.personal')){
    var communications={'tlc':1,'whc':2,'vbc':3,'skc':4,'emc':5,'clc':6};
    var mainInform = debounce(function () {
        var name=$('.per-acc-main-information-input.pa-name').val();
        var phone=$('.per-acc-main-information-input.pa-phone').val();
        var request={}
        if(name){
            request['name']=name;
        }
        if(phone){
            request['phone']=phone;
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            data: request,
            url: '/personal/change',
            success: function (data) {
                alert('success');
            }

        });
    });
    var emailChange = debounce(function () {
        var changeEmail = document.querySelector('.per-acc-main-information-changeemail');
        var email1=$('.per-acc-main-information-input.pa-email.first').val();
        var email2=$('.per-acc-main-information-input.pa-email.repeat').val();
        if(email1===email2) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                data: {email: email1,email_confirmation:email2},
                url: '/personal/change',
                success: function (data) {
                    changeEmail.children[3].value = '';
                    changeEmail.children[1].value = '';
                    alert('success');
                },
                error: function () {
                    changeEmail.children[3].value = '';
                    changeEmail.children[1].value = '';
                }

            });
        }else{
            alert('Emails do not match');
        }
    });
    var passwordChange = debounce(function () {
        var passOld=$('.per-acc-main-information-input.pa-password.old:password').val();
        var pass1=$('.per-acc-main-information-input.pa-password.first:password').val();
        var pass2=$('.per-acc-main-information-input.pa-password.repeat:password').val();
        var changePass = document.querySelector('.per-acc-main-information-changepass');
        if(pass1===pass2) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                data: {passOld: passOld, password: pass1,password_confirmation:pass2},
                url: '/personal/change',
                success: function (data) {
                    changePass.children[3].value = '';
                    changePass.children[1].value = '';
                    changePass.children[5].value = '';
                    alert('success');
                },
                error:function () {
                    changePass.children[3].value = '';
                    changePass.children[1].value = '';
                    changePass.children[5].value = '';
                }

            });
        }else{
            alert('Passwords do not match');
        }

    });
    $('.per-acc-main-information-button').click(mainInform);
    $('.per-acc-main-information-changeemail-button').click(emailChange);
    $('.per-acc-main-information-changepass-button').click(passwordChange);
}
if(!$('body').is('.admin-body')){
    $.validator.addMethod('name', function (value) {
        return /^[a-zA-Z0-9]+$/.test(value);
    }, 'Name must contain only letters and numbers');
    $("#signInBg form").validate();
    $("#register_name").rules("add", {name:true});
    $('.google').click(function () {
        window.location = '/socialite/google';
    });
    $('.facebook').click(function () {
        window.location = '/socialite/facebook';
    });
    var prefer_communication;
    var order_type = 1;
    $('#input1').popover(true, false, 'manual', "", first_form_phone);
    $('.head-main-right-icons-icon').click(function () {

        prefer_communication = $(this).data('id');
    });
    $('.head-main-right-info-item').click(function () {
        order_type = $(this).data('id');
    });
    $('.popup-plans-plan').click(function () {
        order_type = $(this).data('id');
    });
    $('.popupShow1').click(function () {
        order_type = 1;
    });
    $('.popupShow2').click(function () {
        order_type = 2;
    });
    $('.popupShow3').click(function () {
        order_type = 3;
    });
    var headerForm = debounce(function () {
        var phone = $('.head-main-right input[name="phone"]').val();
        var error=[];
        if(!prefer_communication)
            error.push(first_form_com);
        if (error.length === 0) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                data: {client: {phone: phone, prefer: prefer_communication}, order: {type: order_type}},
                url: '/order/pay',
                success: function (data) {
                    if (data.active) {
                        window.location = '/order/pay';
                    } else {
                        var signIn = document.querySelector('#signIn');
                        var logIn = document.querySelector('#logIn');
                        var logInBg = document.querySelector('#logInBg');
                        var logInClose = document.querySelector('#logInClose');
                        logInBg.classList.add('block');
                        document.body.classList.add('body-hidden');
                        document.documentElement.classList.add('html-hidden');
                        window.onclick = function (eve) {
                            if (eve.target == logInBg || eve.target == signInBg) {
                                logInBg.classList.remove('block');
                                signInBg.classList.remove('block');
                                document.body.classList.remove('body-hidden');
                                document.documentElement.classList.remove('html-hidden');
                            }
                        };
                        logInClose.addEventListener('click', function () {
                            logInBg.classList.remove('block');
                            document.body.classList.remove('body-hidden');
                            document.documentElement.classList.remove('html-hidden');
                        });
                        signIn.addEventListener('click', function () {
                            logInBg.classList.remove('block');
                            document.body.classList.remove('body-hidden');
                            document.documentElement.classList.remove('html-hidden');
                            signInScript();
                        });
                    }
                }
            });
        }else{
            alert(error);
        }
    }, 500);
    var popupForm = debounce(function () {
        var phone = $('#input2').val();
        var name = $('#input1').val();
        var description = $('textarea[name="description"]').val();


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                data: {
                    client: {name: name, phone: phone, prefer: prefer_communication},
                    order: {type: order_type, description: description}
                },
                url: '/order/pay',
                success: function (data) {
                    if(data.active) {
                        window.location = '/order/pay';
                    }else {
                        var signIn = document.querySelector('#signIn');
                        var logIn = document.querySelector('#logIn');
                        var logInBg = document.querySelector('#logInBg');
                        var logInClose = document.querySelector('#logInClose');
                        logInBg.classList.add('block');
                        document.body.classList.add('body-hidden');
                        document.documentElement.classList.add('html-hidden');
                        window.onclick = function(eve) {
                            if (eve.target == logInBg || eve.target == signInBg) {
                                logInBg.classList.remove('block');
                                signInBg.classList.remove('block');
                                document.body.classList.remove('body-hidden');
                                document.documentElement.classList.remove('html-hidden');
                            }
                        };
                        logInClose.addEventListener('click', function(){
                            logInBg.classList.remove('block');
                            document.body.classList.remove('body-hidden');
                            document.documentElement.classList.remove('html-hidden');
                        });
                        signIn.addEventListener('click', function(){
                            logInBg.classList.remove('block');
                            document.body.classList.remove('body-hidden');
                            document.documentElement.classList.remove('html-hidden');
                            signInScript();
                        });
                    }
                }
            });

    }, 500);
    var loginForm = debounce(function () {
        var email = $('#login_email').val();
        var password = $('#login_password').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                data: {email: email, password: password},
                url: '/login',
                success: function (data) {
                    if(data.success){
                        location.reload();
                    }else{
                        alert(data.error);
                    }
                },
                error:function (data) {
                    $.each(data.responseJSON.errors, function( index, value ) {
                        alert(value);
                    });
                }
            });

    }, 500);
    var registerForm = debounce(function () {
        var name = $('#register_name').val();
        var email = $('#register_email').val();
        var password = $('#register_password1').val();
        var rep_password = $('#register_password2').val();
        var error = [];
        if(password!==rep_password)
            error.push(register_not_matched);

        if (error.length === 0) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                data: {name:name,email: email, password: password,password_confirmation:password},
                url: '/register',
                success: function (data) {
                        location.reload();

                },
                error:function (data) {
                    $.each(data.responseJSON.errors, function( index, value ) {
                        alert(value);
                    });
                }
            });
        } else {
            alert(error);
            /* $.each(error, function( index, value ) {
             console.log(value);
             $('.head-main-right .alert_block').append('<div class="alert alert-danger" role="alert">'+value);
             });*/
        }
    }, 500);
    $('.head-main-right form').submit(function(e){
        e.preventDefault();
        headerForm();
    });
    $('body').on('submit', '.popup-bg form', function(e){
        e.preventDefault();
        popupForm();
    });
    $('#logInBg').on('submit', 'form', function(e){
        e.preventDefault();
        loginForm();
    });
    $('#signInBg').on('submit', 'form', function(e){
        e.preventDefault();
        registerForm();
    });
}
    $('.header-right-logout').click(function () {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '/personal/logout',
            success: function (data) {
                location.replace('/');
            }
        });
        /*$('#logout-form').submit();*/
    });
if($('div').is('.blog_main')) {
    var searchBlog =debounce(function() {
        if (this.string.length > 3) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                data:{sections:['blog'],string:this.string},
                url: '/blog/search',
                success: function (data) {
                    if(data) {
                        window.location = '/blog/search/'+JSON.stringify(data.blog);
                    }
                }.bind(this)
            });
        }
    }, 1000);
    var search = new Vue({
        el: '#search',
        data: {
            string:''
        },
        methods: {
            search: searchBlog
        }
    });

    /*var myHeavyFunction =debounce(function() {
        if (this.string.length > 3||this.string.length===0) {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                data:{sections:['blog'],string:this.string},
                url: '/blog',
                success: function (data) {
                    this.articles=data;
                    console.log(articles);
                }.bind(this)
            });
        }
    }, 1000);
    Vue.component('todo-item', {
        template: '<div class="main-articles-article">\
        <img :src="image" alt="img-1"><div class="main-articles-article-title">{{ title }}</div>\
    <div class="main-articles-article-text">{{ text }}</div>\
    </div>',
        props: ['image','title','text']
    });
    var blog = new Vue({
        el: '#blog',
        data: {
            articles:articles,
            string:''
        },
        methods: {
            create_casenr: myHeavyFunction

        }
    });
    $('input[name="string"]').keyup(function () {
        var string = $(this).val();

    });*/
}
$('#input1').keyup(function () {
    $('.signin-container input[placeholder="Enter your name"]').val($(this).val());
});
function payRedirrect() {
    window.location = '/order/pay';
}
function debounce (func, interval) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            func.apply(context, args);
        };
        clearTimeout(timeout);
        timeout = setTimeout(later, interval || 200);
    }
}